<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package 7up-framework
 */

get_header(); ?>
<div class="main-wrapper st-default content-page"> 
    <div class="container">
    <?php s7upf_display_breadcrumb();?>
        <div class="row">
            <?php s7upf_output_sidebar('left')?>
            <div class="blog-list <?php echo esc_attr(s7upf_get_main_class()); ?>">                    
                <?php the_archive_title('<h2 class="page-title">','</h2>'); ?>
                <?php if(have_posts()):?>    
                    <?php while (have_posts()) :the_post();?>
                        <?php get_template_part('s7upf_templates/blog-content/content')?>
                    <?php endwhile;?>
                    <?php wp_reset_postdata();?>

                    <?php s7upf_paging_nav();?>

                <?php else : ?>
                    <?php get_template_part( 's7upf_templates/blog-content/content', 'none' ); ?>
                <?php endif;?>
            </div>
            <?php s7upf_output_sidebar('right')?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
