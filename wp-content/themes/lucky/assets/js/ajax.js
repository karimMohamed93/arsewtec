(function($){
    "use strict";

    //Shop Filter
    function get_shop_filter(seff){
        var filter = {};
        filter['price'] = {};
        filter['cats'] = [];
        filter['attributes'] = {};
        var terms = [];
        var min_price = $('#min_price').attr('data-min');
        var max_price = $('#max_price').attr('data-max');
        filter['min_price'] = min_price;
        filter['max_price'] = max_price;
        seff.toggleClass('active');
        if(seff.hasClass('page-numbers')){
            seff.parents('.page-numbers').find('.page-numbers').not(seff).removeClass('current');
            seff.parents('.page-numbers').find('.page-numbers').not(seff).removeClass('active');
            seff.addClass('current');
            seff.addClass('active');
        }
        else{
            $('.page-numbers').removeClass('current');
            $('.page-numbers').removeClass('active');
            $('.page-numbers li').first().find('.page-numbers').addClass('current active');
        }
        if(seff.attr('data-type')) seff.parents('.product-sort').find('a.load-shop-ajax').not(seff).removeClass('active');        
        if($('.price_label .from')) filter['price']['min'] = $('#min_price').val();
        if($('.price_label .to')) filter['price']['max'] = $('#max_price').val();
        if($('.woocommerce-ordering')) filter['orderby'] = $('select[name="orderby"]').val();
        if($('.page-numbers.current')) filter['page'] = $('.page-numbers.current').html();
        if($('.page-numbers.active')) filter['page'] = $('.page-numbers.active').html();
        if($('.list-product').attr('data-number')) filter['number'] = $('.list-product').attr('data-number');
        if($('.list-product').attr('data-column')) filter['column'] = $('.list-product').attr('data-column');
        var i = 1;
        $('.load-shop-ajax.active').each(function(){
            var seff2 = $(this);
            if(seff2.attr('data-type')){
                if(i == 1) filter['type'] = seff2.attr('data-type');
                i++;
            }
            if(seff2.attr('data-attribute') && seff2.attr('data-term')){
                if(!filter['attributes'][seff2.attr('data-attribute')]) filter['attributes'][seff2.attr('data-attribute')] = [];
                if($.inArray(seff2.attr('data-term'),filter['attributes'][seff2.attr('data-attribute')])) filter['attributes'][seff2.attr('data-attribute')].push(seff2.attr('data-term'));
            }
            if(seff2.attr('data-cat') && $.inArray(seff2.attr('data-cat'),filter['cats'])) filter['cats'].push(seff2.attr('data-cat'));
        })
        if($('.shop-page').attr('data-cats')) filter['cats'].push($('.shop-page').attr('data-cats'));
        // console.log(filter['attributes']);
        return filter;
    }
    function load_ajax_shop(e){
        e.preventDefault();
        var filter = get_shop_filter($(this));
        var content = $('.main-shop-load');
        content.addClass('loadding');
        content.append('<div class="ajax-loading"><i class="fa fa-spinner fa-spin"></i></div>');
        $.ajax({
            type : "post",
            url : ajax_process.ajaxurl,
            crossDomain: true,
            data: {
                action: "load_shop",
                filter_data: filter,
            },
            success: function(data){
                if(data[data.length-1] == '0' ){
                    data = data.split('');
                    data[data.length-1] = '';
                    data = data.join('');
                }
                content.find(".ajax-loading").remove();
                content.removeClass('loadding');
                content.html(data);
            },
            error: function(MLHttpRequest, textStatus, errorThrown){                    
                console.log(errorThrown);  
            }
        });
        console.log(filter);
        return false;
    }
    //Product Filter
    function get_filter(seff){
        var filter = {};
        // filter['tab'] = seff.parents('.filter-show').find('.title-tab1 li.active a').attr('data-tab');
        seff.parents('.filter-show').find('ul.attr-list').each(function(){
            var key = $(this).attr('data-key');
            filter[key]=[];
            $(this).find('li a').each(function(){
                if($(this).hasClass('selected')) filter[key].push($(this).attr('data-value'));
            })
        })
        filter['price'] = seff.parents('.filter-show').find( ".get-filter-price" ).val();
        return filter;
    }

    $(document).ready(function() {
        // Live search
        $('.live-search-on input[name="s"]').on('click',function(event){
            event.preventDefault();
            event.stopPropagation();
            $(this).parents('.live-search-on').addClass('active');
        })
        $('body').on('click',function(event){
            $('.live-search-on.active').removeClass('active');
        })
        $('.live-search-on input[name="s"]').on('change keyup',function(){
           var key = $(this).val();
           var trim_key = key.trim();
           if(key && trim_key){
               var cat = $(this).parents('.live-search-on').find('.cat-value').val();
               var taxonomy = $(this).parents('.live-search-on').find('.cat-value').attr("name");
               var post_type = $(this).parents('.live-search-on').find('input[name="post_type"]').val();
               var seff = $(this);
               var content = seff.parent().find('.list-product-search');
               content.html('<i class="fa fa-spinner fa-spin"></i>');
               content.addClass('search-loading');
               $.ajax({
                    type : "post",
                    url : ajax_process.ajaxurl,
                    crossDomain: true,
                    data: {
                        action: "live_search",
                        key: key,
                        cat: cat,
                        post_type: post_type,
                        taxonomy: taxonomy,
                    },
                    success: function(data){
                        content.removeClass('search-loading');
                        if(data[data.length-1] == '0' ){
                            data = data.split('');
                            data[data.length-1] = '';
                            data = data.join('');
                        }
                        content.html(data);
                    },
                    error: function(MLHttpRequest, textStatus, errorThrown){                    
                        console.log(errorThrown);  
                    }
                });
           }
        })

        // Shop ajax
        $('.shop-ajax-enable').on('click','.load-shop-ajax,.page-numbers,.price_slider_amount .button',load_ajax_shop);
        $('.shop-ajax-enable').on('change','select[name="orderby"]',load_ajax_shop);
        $( '.shop-ajax-enable .woocommerce-ordering' ).on( 'submit', function(e) {
            e.preventDefault();
        });
        // Wishlist ajax
        $('.wishlist-close').on('click',function(){
            $('.wishlist-mask').fadeOut();
        })
        $('.add_to_wishlist').live('click',function(){
            $(this).addClass('added');
            var product_id = $(this).attr("data-product-id");
            var product_title = $(this).attr("data-product-title");
            $('.wishlist-title').html(product_title);
            $('.wishlist-mask').fadeIn();
            var counter = 3;
            var popup;
            popup = setInterval(function() {
                counter--;
                if(counter < 0) {
                    clearInterval(popup);
                    $('.wishlist-mask').hide();
                } else {
                    $(".wishlist-countdown").text(counter.toString());
                }
            }, 1000);
        })
        /// Woocommerce Ajax
        $("body").on("click",".add_to_cart_button:not(.product_type_variable)",function(e){
            e.preventDefault();
            var product_id = $(this).attr("data-product_id");
            var seff = $(this);
            seff.append('<i class="fa fa-spinner fa-spin"></i>');
            $.ajax({
                type : "post",
                url : ajax_process.ajaxurl,
                crossDomain: true,
                data: {
                    action: "add_to_cart",
                    product_id: product_id
                },
                success: function(data){
                    seff.find('.fa-spinner').remove();
                    var cart_content = data.fragments['div.widget_shopping_cart_content'];
                    // console.log(data.fragments['div.widget_shopping_cart_content']);
                    $('.mini-cart-content').html(cart_content);
                    var count_item = cart_content.split("<li").length;
                    $('.cart-item-count').html(count_item-1);
                    // $('.number-cart-total').html(count_item-1);
                    var price = $('.mini-cart-content').find('.total-cart').find('.total-price').html();
                    $('.mini-cart-total').html(price);
                    if($('.info-list-cart').length > 0){
                        if($('.info-list-cart').height() >= 245) $('.info-list-cart').mCustomScrollbar();
                    }
                },
                error: function(MLHttpRequest, textStatus, errorThrown){                    
                    console.log(errorThrown);  
                }
            });
        });

        $('body').on('click', '.btn-remove', function(e){
            e.preventDefault();
            var cart_item_key = $(this).parents('.item-info-cart').attr("data-key");
            var element = $(this).parents('.item-info-cart');
            var currency = ["د.إ","лв.","kr.","Kr.","Rs.","руб."];
            var decimal = $("#num-decimal").val();
            function get_currency(pricehtml){
                var check,index,price,i;
                for(i = 0;i<6;i++){
                    if(pricehtml.search(currency[i]) != -1)  {
                        check = true;
                        index = i;
                    }
                }
                if(check) price =  pricehtml.replace(currency[index],"");
                else price = pricehtml.replace(/[^0-9\.]+/g,"");
                return price;
            }
            $.ajax({
                type: 'POST',
                url: ajax_process.ajaxurl,                
                crossDomain: true,
                data: { 
                    action: 'product_remove',
                    cart_item_key: cart_item_key
                },
                success: function(data){
                    console.log(data);
                    var price_html = element.find('span.amount').html();
                    var price = get_currency(price_html);
                    var qty = element.find('.cart-qty').find('span').html();
                    var price_remove = price*qty;
                    var current_total_html = $(".total-price").find(".amount").html();
                    console.log(current_total_html);
                    var current_total = get_currency(current_total_html);
                    var new_total = current_total-price_remove;
                    new_total = parseFloat(new_total).toFixed(decimal);
                    current_total_html = current_total_html.replace(',','');
                    var new_total_html = current_total_html.replace(current_total,new_total);
                    element.slideUp().remove();
                    // console.log(current_total);
                    // console.log(new_total);
                    // console.log(new_total_html);
                    $(".total-price").find(".amount").html(new_total_html);
                    var current_html = $('.cart-item-count').html();
                    $('.cart-item-count').html(current_html-1);
                },
                error: function(MLHttpRequest, textStatus, errorThrown){  
                    console.log(errorThrown);  
                }
            });
            return false;
        });

        $('body').on('click','.product-quick-view', function(e){            
            $.fancybox.showLoading();
            var product_id = $(this).attr('data-product-id');
            $.ajax({
                type: 'POST',
                url: ajax_process.ajaxurl,                
                crossDomain: true,
                data: { 
                    action: 'product_popup_content',
                    product_id: product_id
                },
                success: function(res){
                    // console.log(res);
                    if(res[res.length-1] == '0' ){
                        res = res.split('');
                        res[res.length-1] = '';
                        res = res.join('');
                    }
                    $.fancybox.hideLoading();
                    $.fancybox(res, {
                        width: 1000,
                        height: 600,
                        autoSize: false,
                        onStart: function(opener) {                            
                            if ($(opener).attr('id') == 'login') {
                                $.get('/hicommon/authenticated', function(res) { 
                                    if ('yes' == res) {
                                      console.log('this user must have already authenticated in another browser tab, SO I want to avoid opening the fancybox.');
                                      return false;
                                    } else {
                                      console.log('the user is not authenticated');
                                      return true;
                                    }
                                }); 
                            }
                        },
                    });
                    
                    
/*!
 * Variations Plugin
 */
!function(a,b,c,d){a.fn.wc_variation_form=function(){var c=this,f=c.closest(".product"),g=parseInt(c.data("product_id"),10),h=c.data("product_variations"),i=h===!1,j=!1,k=c.find(".reset_variations");return c.unbind("check_variations update_variation_values found_variation"),c.find(".reset_variations").unbind("click"),c.find(".variations select").unbind("change focusin"),c.on("click",".reset_variations",function(){return c.find(".variations select").val("").change(),c.trigger("reset_data"),!1}).on("reload_product_variations",function(){h=c.data("product_variations"),i=h===!1}).on("reset_data",function(){var b={".sku":"o_sku",".product_weight":"o_weight",".product_dimensions":"o_dimensions"};a.each(b,function(a,b){var c=f.find(a);c.attr("data-"+b)&&c.text(c.attr("data-"+b))}),c.wc_variations_description_update(""),c.trigger("reset_image"),c.find(".single_variation_wrap").slideUp(200).trigger("hide_variation")}).on("reset_image",function(){var a=f.find("div.images img:eq(0)"),b=f.find("div.images a.zoom:eq(0)"),c=a.attr("data-o_src"),e=a.attr("data-o_title"),g=a.attr("data-o_title"),h=b.attr("data-o_href");c!==d&&a.attr("src",c),h!==d&&b.attr("href",h),e!==d&&(a.attr("title",e),b.attr("title",e)),g!==d&&a.attr("alt",g)}).on("change",".variations select",function(){if(c.find('input[name="variation_id"], input.variation_id').val("").change(),c.find(".wc-no-matching-variations").remove(),i){j&&j.abort();var b=!0,d=!1,e={};c.find(".variations select").each(function(){var c=a(this).data("attribute_name")||a(this).attr("name");0===a(this).val().length?b=!1:d=!0,e[c]=a(this).val()}),b?(e.product_id=g,j=a.ajax({url:wc_cart_fragments_params.wc_ajax_url.toString().replace("%%endpoint%%","get_variation"),type:"POST",data:e,success:function(a){a?(c.find('input[name="variation_id"], input.variation_id').val(a.variation_id).change(),c.trigger("found_variation",[a])):(c.trigger("reset_data"),c.find(".single_variation_wrap").after('<p class="wc-no-matching-variations woocommerce-info">'+wc_add_to_cart_variation_params.i18n_no_matching_variations_text+"</p>"),c.find(".wc-no-matching-variations").slideDown(200))}})):c.trigger("reset_data"),d?"hidden"===k.css("visibility")&&k.css("visibility","visible").hide().fadeIn():k.css("visibility","hidden")}else c.trigger("woocommerce_variation_select_change"),c.trigger("check_variations",["",!1]),a(this).blur();c.trigger("woocommerce_variation_has_changed")}).on("focusin touchstart",".variations select",function(){i||(c.trigger("woocommerce_variation_select_focusin"),c.trigger("check_variations",[a(this).data("attribute_name")||a(this).attr("name"),!0]))}).on("found_variation",function(a,b){var e=f.find("div.images img:eq(0)"),g=f.find("div.images a.zoom:eq(0)"),h=e.attr("data-o_src"),i=e.attr("data-o_title"),j=e.attr("data-o_alt"),k=g.attr("data-o_href"),l=b.image_src,m=b.image_link,n=b.image_caption,o=b.image_title;c.find(".single_variation").html(b.price_html+b.availability_html),h===d&&(h=e.attr("src")?e.attr("src"):"",e.attr("data-o_src",h)),k===d&&(k=g.attr("href")?g.attr("href"):"",g.attr("data-o_href",k)),i===d&&(i=e.attr("title")?e.attr("title"):"",e.attr("data-o_title",i)),j===d&&(j=e.attr("alt")?e.attr("alt"):"",e.attr("data-o_alt",j)),l&&l.length>1?(e.attr("src",l).attr("alt",o).attr("title",o),g.attr("href",m).attr("title",n)):(e.attr("src",h).attr("alt",j).attr("title",i),g.attr("href",k).attr("title",i));var p=c.find(".single_variation_wrap"),q=f.find(".product_meta").find(".sku"),r=f.find(".product_weight"),s=f.find(".product_dimensions");q.attr("data-o_sku")||q.attr("data-o_sku",q.text()),r.attr("data-o_weight")||r.attr("data-o_weight",r.text()),s.attr("data-o_dimensions")||s.attr("data-o_dimensions",s.text()),b.sku?q.text(b.sku):q.text(q.attr("data-o_sku")),b.weight?r.text(b.weight):r.text(r.attr("data-o_weight")),b.dimensions?s.text(b.dimensions):s.text(s.attr("data-o_dimensions"));var t=!1,u=!1;b.is_purchasable&&b.is_in_stock&&b.variation_is_visible||(u=!0),b.variation_is_visible||c.find(".single_variation").html("<p>"+wc_add_to_cart_variation_params.i18n_unavailable_text+"</p>"),""!==b.min_qty?p.find(".quantity input.qty").attr("min",b.min_qty).val(b.min_qty):p.find(".quantity input.qty").removeAttr("min"),""!==b.max_qty?p.find(".quantity input.qty").attr("max",b.max_qty):p.find(".quantity input.qty").removeAttr("max"),"yes"===b.is_sold_individually&&(p.find(".quantity input.qty").val("1"),t=!0),t?p.find(".quantity").hide():u||p.find(".quantity").show(),u?p.is(":visible")?c.find(".variations_button").slideUp(200):c.find(".variations_button").hide():p.is(":visible")?c.find(".variations_button").slideDown(200):c.find(".variations_button").show(),c.wc_variations_description_update(b.variation_description),p.slideDown(200).trigger("show_variation",[b])}).on("check_variations",function(c,d,f){if(!i){var g=!0,j=!1,k={},l=a(this),m=l.find(".reset_variations");l.find(".variations select").each(function(){var b=a(this).data("attribute_name")||a(this).attr("name");0===a(this).val().length?g=!1:j=!0,d&&b===d?(g=!1,k[b]=""):k[b]=a(this).val()});var n=e.find_matching_variations(h,k);if(g){var o=n.shift();o?(l.find('input[name="variation_id"], input.variation_id').val(o.variation_id).change(),l.trigger("found_variation",[o])):(l.find(".variations select").val(""),f||l.trigger("reset_data"),b.alert(wc_add_to_cart_variation_params.i18n_no_matching_variations_text))}else l.trigger("update_variation_values",[n]),f||l.trigger("reset_data"),d||l.find(".single_variation_wrap").slideUp(200).trigger("hide_variation");j?"hidden"===m.css("visibility")&&m.css("visibility","visible").hide().fadeIn():m.css("visibility","hidden")}}).on("update_variation_values",function(b,d){i||(c.find(".variations select").each(function(b,c){var e,f=a(c);f.data("attribute_options")||f.data("attribute_options",f.find("option:gt(0)").get()),f.find("option:gt(0)").remove(),f.append(f.data("attribute_options")),f.find("option:gt(0)").removeClass("attached"),f.find("option:gt(0)").removeClass("enabled"),f.find("option:gt(0)").removeAttr("disabled"),e="undefined"!=typeof f.data("attribute_name")?f.data("attribute_name"):f.attr("name");for(var g in d)if("undefined"!=typeof d[g]){var h=d[g].attributes;for(var i in h)if(h.hasOwnProperty(i)){var j=h[i];if(i===e){var k="";d[g].variation_is_active&&(k="enabled"),j?(j=a("<div/>").html(j).text(),j=j.replace(/'/g,"\\'"),j=j.replace(/"/g,'\\"'),f.find('option[value="'+j+'"]').addClass("attached "+k)):f.find("option:gt(0)").addClass("attached "+k)}}}f.find("option:gt(0):not(.attached)").remove(),f.find("option:gt(0):not(.enabled)").attr("disabled","disabled")}),c.trigger("woocommerce_update_variation_values"))}),c.trigger("wc_variation_form"),c};var e={find_matching_variations:function(a,b){for(var c=[],d=0;d<a.length;d++){var f=a[d];e.variations_match(f.attributes,b)&&c.push(f)}return c},variations_match:function(a,b){var c=!0;for(var e in a)if(a.hasOwnProperty(e)){var f=a[e],g=b[e];f!==d&&g!==d&&0!==f.length&&0!==g.length&&f!==g&&(c=!1)}return c}};a.fn.wc_variations_description_update=function(b){var c=this,d=c.find(".woocommerce-variation-description");if(0===d.length)b&&(c.find(".single_variation_wrap").prepend(a('<div class="woocommerce-variation-description" style="border:1px solid transparent;">'+b+"</div>").hide()),c.find(".woocommerce-variation-description").slideDown(200));else{var e=d.outerHeight(!0),f=0,g=!1;d.css("height",e),d.html(b),d.css("height","auto"),f=d.outerHeight(!0),Math.abs(f-e)>1&&(g=!0,d.css("height",e)),g&&d.animate({height:f},{duration:200,queue:!1,always:function(){d.css({height:"auto"})}})}},a(function(){"undefined"!=typeof wc_add_to_cart_variation_params&&a(".variations_form").each(function(){a(this).wc_variation_form().find(".variations select:eq(0)").change()})})}(jQuery,window,document);
                    
                    //Product Gallery
                    if($('.product-gallery .bxslider').length>0){
                        $('.product-gallery .bxslider').bxSlider({
                            pagerCustom: '.product-gallery #bx-pager',
                            nextText:'<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            prevText:'<i class="fa fa-angle-left" aria-hidden="true"></i>'
                        });
                    }
                    $('input[name="variation_id"]').on('change',function(){
                        var id = $(this).val();
                        var data = $('.variations_form').attr('data-product_variations');
                        var curent_data = {};
                        data = $.parseJSON(data);
                        if(id){
                            for (var i = data.length - 1; i >= 0; i--) {
                                if(data[i].variation_id == id) curent_data = data[i];
                                if(data[i].is_in_stock) $('.product-available .avail-instock').html($('.product-available').attr('data-instock')).css("color","#72b226");
                                else $('.product-available .avail-instock').html($('.product-available').attr('data-outstock')).css("color","#ff0000");
                                $('.product-code span').html(data[i].sku);
                            };
                            if('image_id' in curent_data){
                                $('.product-gallery #bx-pager').find('a[data-image_id="'+curent_data.image_id+'"]').trigger( 'click' );
                            }
                        }          
                    })
                    //QUANTITY CLICK
                    $(".quantity").find(".qty-up").on("click",function(){
                        var min = $(this).prev().attr("min");
                        var max = $(this).prev().attr("max");
                        var step = $(this).prev().attr("step");
                        if(step === undefined) step = 1;
                        if(max !==undefined && Number($(this).prev().val())< Number(max) || max === undefined){ 
                            if(step!='') $(this).prev().val(Number($(this).prev().val())+Number(step));
                        }
                        return false;
                    })
                    $(".quantity").find(".qty-down").on("click",function(){
                        var min = $(this).next().attr("min");
                        var max = $(this).next().attr("max");
                        var step = $(this).next().attr("step");
                        if(step === undefined) step = 1;
                        if(Number($(this).next().val()) > 1){
                            if(min !==undefined && $(this).next().val()>min || min === undefined){
                                if(step!='') $(this).next().val(Number($(this).next().val())-Number(step));
                            }
                        }
                        return false;
                    })
                    $("input.qty-val").on("keyup change",function(){
                        var max = $(this).attr('max');
                        if( Number($(this).val()) > Number(max) ) $(this).val(max);
                    })
                    //END
                    // variable product
                    if($('.wrap-attr-product1.special').length > 0){
                        $('.attr-filter ul li a').live('click',function(event){
                            event.preventDefault();
                            $(this).parents('ul').find('li').removeClass('active');
                            $(this).parent().addClass('active');
                            var attribute = $(this).parent().attr('data-attribute');
                            var id = $(this).parents('ul').attr('data-attribute-id');
                            $('#'+id).val(attribute);
                            $('#'+id).trigger( 'change' );
                            $('#'+id).trigger( 'focusin' );
                            return false;
                        })
                        $('.attr-hover-box').hover(function(){
                            var seff = $(this);
                            var old_html = $(this).find('ul').html();
                            var current_val = $(this).find('ul li.active').attr('data-attribute');
                            $(this).next().find('select').trigger( 'focusin' );
                            var content = '';
                            $(this).next().find('select').find('option').each(function(){
                                var val = $(this).attr('value');
                                var title = $(this).html();
                                var el_class = '';
                                if(current_val == val) el_class = ' class="active"';
                                if(val != ''){
                                    content += '<li'+el_class+' data-attribute="'+val+'"><a href="#" class="bgcolor-'+val+'"><span></span>'+title+'</a></li>';
                                }
                            })
                            // console.log(content);
                            if(old_html != content) $(this).find('ul').html(content);
                        })
                        $('body .reset_variations').live('click',function(){
                            $('.attr-hover-box').each(function(){
                                var seff = $(this);
                                var old_html = $(this).find('ul').html();
                                var current_val = $(this).find('ul li.active').attr('data-attribute');
                                $(this).next().find('select').trigger( 'focusin' );
                                var content = '';
                                $(this).next().find('select').find('option').each(function(){
                                    var val = $(this).attr('value');
                                    var title = $(this).html();
                                    var el_class = '';
                                    if(current_val == val) el_class = ' class="active"';
                                    if(val != ''){
                                        content += '<li'+el_class+' data-attribute="'+val+'"><a href="#" class="bgcolor-'+val+'"><span></span>'+title+'</a></li>';
                                    }
                                })
                                if(old_html != content) $(this).find('ul').html(content);
                                $(this).find('ul li').removeClass('active');
                            })
                        })
                    }
                },
                error: function(MLHttpRequest, textStatus, errorThrown){  
                    console.log(errorThrown);  
                }
            });        
            return false;
        })

        /// Load product Ajax
        $("body").on("click",".ajax-loadmore-show .load-ajax-btn",function(e){
            e.preventDefault();
            var item_style = $(this).prev('.content-style-list').attr('class');
            var page = $(this).attr("data-page");
            var max_page = $(this).attr("data-max_page");
            var load_data = $(this).attr("data-load_data");
            var seff = $(this);
            var filter_data = get_filter(seff);
            console.log(filter_data);
            var content = seff.parents('.content-load-wrap').find('.content-load-ajax');
            seff.find('i').addClass('fa-spin');
            $.ajax({
                type : "post",
                url : ajax_process.ajaxurl,
                crossDomain: true,
                data: {
                    action: "loadmore_product",
                    load_data: load_data,
                    page: page,
                    filter_data: filter_data,
                },
                success: function(data){
                    if(data[data.length-1] == '0' ){
                        data = data.split('');
                        data[data.length-1] = '';
                        data = data.join('');
                    }
                    // console.log(data);
                    if(item_style == 'content-style-list list-product4'){
                        var $newItem = $(data);
                        content.append($newItem).masonry( 'appended', $newItem, true );
                        content.imagesLoaded( function() {
                            content.masonry('layout');
                        });
                    }
                    else content.append(data);
                    seff.find('i').removeClass('fa-spin');
                    page = Number(page) +1;
                    seff.attr('data-page',page);
                    if(page >= Number(max_page)) seff.fadeOut();
                },
                error: function(MLHttpRequest, textStatus, errorThrown){                    
                    console.log(errorThrown);  
                }
            });
        });
        /// Filter product Ajax
        $("body").on("click",".filter-show .filter-ajax",function(e){
            e.preventDefault();
            var seff = $(this);
            var filter_data = get_filter(seff);
            var id = $(this).attr('href');
            if(id && id != "#"){
                $(this).parent().parent().find('li').removeClass('active');
                $(this).parent().addClass('active');
                $(this).parents('.filter-show').find('.tab-pane').removeClass('active');
                $(id).addClass('active');
            }
            var load_data = $(this).parents('.filter-show').find('.tab-pane.active .load-ajax-btn').attr("data-load_data");
            // console.log(filter_data);
            var content = seff.parents('.filter-show').find('.tab-pane.active');
            var item_style = seff.parents('.filter-show').find('.content-style-list').attr('class');
            seff.find('i').addClass('fa-spin');
            content.addClass('loadding');
            content.append('<div class="ajax-loading"><i class="fa fa-spinner fa-spin"></i></div>');
            $.ajax({
                type : "post",
                url : ajax_process.ajaxurl,
                crossDomain: true,
                data: {
                    action: "filter_product",
                    load_data: load_data,
                    filter_data: filter_data,
                },
                success: function(data){
                    if(data == '0' || data == 0 || data == ''){
                        content.find('.no-product').removeClass('hidden');
                    }
                    else{
                        if(!content.find('.no-product').hasClass('hidden')) content.parent().find('.no-product').addClass('hidden');
                    }
                    if(data[data.length-1] == '0' ){
                        data = data.split('');
                        data[data.length-1] = '';
                        data = data.join('');
                    }
                    content.find(".ajax-loading").remove();
                    content.removeClass('loadding');
                    // console.log(data);
                    content.html(data);
                    if(item_style == 'content-style-list list-product4'){
                        //Product Masonry 
                        if($('.list-product4').length>0){
                            $('.list-product4 > div').imagesLoaded( function(){
                                $('.list-product4 > div').masonry({
                                    // options
                                    itemSelector: '.item-product-masonry',
                                });
                            });
                        }
                    }
                },
                error: function(MLHttpRequest, textStatus, errorThrown){                    
                    console.log(errorThrown);  
                }
            });
        });
    });//End ready

})(jQuery);