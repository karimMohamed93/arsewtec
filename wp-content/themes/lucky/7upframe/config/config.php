<?php
/**
 * Created by Sublime Text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:20 AM
 */
if(!function_exists('s7upf_set_theme_config')){
    function s7upf_set_theme_config(){
        global $s7upf_dir,$s7upf_config;
        /**************************************** BEGIN ****************************************/
        $s7upf_dir = get_template_directory_uri() . '/7upframe';
        $s7upf_config = array();
        $s7upf_config['dir'] = $s7upf_dir;
        $s7upf_config['css_url'] = $s7upf_dir . '/assets/css/';
        $s7upf_config['js_url'] = $s7upf_dir . '/assets/js/';
        $s7upf_config['nav_menu'] = array(
            'primary' => esc_html__( 'Primary Navigation', 'lucky' ),
        );
        $s7upf_config['mega_menu'] = '1';
        $s7upf_config['sidebars']=array(
            array(
                'name'              => esc_html__( 'Blog Sidebar', 'lucky' ),
                'id'                => 'blog-sidebar',
                'description'       => esc_html__( 'Widgets in this area will be shown on all blog page.', 'lucky'),
                'before_title'      => '<h3 class="widget-title">',
                'after_title'       => '</h3>',
                'before_widget'     => '<div id="%1$s" class="sidebar-widget widget %2$s">',
                'after_widget'      => '</div>',
            )
        );
        $s7upf_config['import_config'] = array(
                'homepage_default'          => 'Home',
                'blogpage_default'          => 'Blog',
                'menu_locations'            => array("Main Menu" => "primary"),
                'set_woocommerce_page'      => 1
            );
        $s7upf_config['import_theme_option'] = 'YTo0MDp7czoxNzoiczd1cGZfaGVhZGVyX3BhZ2UiO3M6MzoiOTgwIjtzOjE3OiJzN3VwZl9mb290ZXJfcGFnZSI7czozOiI5ODMiO3M6MTQ6InM3dXBmXzQwNF9wYWdlIjtzOjA6IiI7czoxNjoic2hvd19oZWFkZXJfcGFnZSI7czoyOiJvbiI7czoxNzoiaGVhZGVyX3BhZ2VfaW1hZ2UiO2E6Mjp7aTowO2E6Mzp7czo1OiJ0aXRsZSI7czo3OiJTbGlkZSAxIjtzOjEyOiJoZWFkZXJfaW1hZ2UiO3M6NzM6Imh0dHA6Ly83dXB0aGVtZS5jb20vd29yZHByZXNzL2x1Y2t5L3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE2LzExL3NsaWRlMS5qcGciO3M6MTE6ImhlYWRlcl9saW5rIjtzOjE6IiMiO31pOjE7YTozOntzOjU6InRpdGxlIjtzOjc6IlNsaWRlIDIiO3M6MTI6ImhlYWRlcl9pbWFnZSI7czo3MzoiaHR0cDovLzd1cHRoZW1lLmNvbS93b3JkcHJlc3MvbHVja3kvd3AtY29udGVudC91cGxvYWRzLzIwMTYvMTEvc2xpZGUyLmpwZyI7czoxMToiaGVhZGVyX2xpbmsiO3M6MToiIyI7fX1zOjIwOiJzN3VwZl9zaG93X2JyZWFkcnVtYiI7czoyOiJvbiI7czoxOToiczd1cGZfYmdfYnJlYWRjcnVtYiI7czowOiIiO3M6MTU6InNob3dfc2Nyb2xsX3RvcCI7czozOiJvZmYiO3M6MTA6Im1haW5fY29sb3IiO3M6MDoiIjtzOjExOiJtYXBfYXBpX2tleSI7czozOToiQUl6YVN5QlgySWlFQmctMGxRS1FRNndrNnNXUkdRbldJN2lvZ2YwIjtzOjEwOiJjdXN0b21fY3NzIjtzOjA6IiI7czo0OiJsb2dvIjtzOjcxOiJodHRwOi8vN3VwdGhlbWUuY29tL3dvcmRwcmVzcy9sdWNreS93cC1jb250ZW50L3VwbG9hZHMvMjAxNi8xMS9sb2dvLnBuZyI7czo3OiJmYXZpY29uIjtzOjcwOiJodHRwOi8vN3VwdGhlbWUuY29tL3dvcmRwcmVzcy9sdWNreS93cC1jb250ZW50L3VwbG9hZHMvMjAxNi8xMS83dXAuanBnIjtzOjE2OiJzN3VwZl9tZW51X2ZpeGVkIjtzOjI6Im9uIjtzOjE2OiJzN3VwZl9tZW51X2NvbG9yIjtzOjA6IiI7czoyMjoiczd1cGZfbWVudV9jb2xvcl9ob3ZlciI7czowOiIiO3M6MjM6InM3dXBmX21lbnVfY29sb3JfYWN0aXZlIjtzOjA6IiI7czoyNzoiczd1cGZfc2lkZWJhcl9wb3NpdGlvbl9ibG9nIjtzOjU6InJpZ2h0IjtzOjE4OiJzN3VwZl9zaWRlYmFyX2Jsb2ciO3M6MTI6ImJsb2ctc2lkZWJhciI7czoyNzoiczd1cGZfc2lkZWJhcl9wb3NpdGlvbl9wYWdlIjtzOjI6Im5vIjtzOjE4OiJzN3VwZl9zaWRlYmFyX3BhZ2UiO3M6MDoiIjtzOjM1OiJzN3VwZl9zaWRlYmFyX3Bvc2l0aW9uX3BhZ2VfYXJjaGl2ZSI7czo1OiJyaWdodCI7czoyNjoiczd1cGZfc2lkZWJhcl9wYWdlX2FyY2hpdmUiO3M6MTI6ImJsb2ctc2lkZWJhciI7czoyNzoiczd1cGZfc2lkZWJhcl9wb3NpdGlvbl9wb3N0IjtzOjI6Im5vIjtzOjE4OiJzN3VwZl9zaWRlYmFyX3Bvc3QiO3M6MDoiIjtzOjE3OiJzN3VwZl9hZGRfc2lkZWJhciI7YToyOntpOjA7YToyOntzOjU6InRpdGxlIjtzOjE5OiJXb29jb21tZXJjZSBTaWRlYmFyIjtzOjIwOiJ3aWRnZXRfdGl0bGVfaGVhZGluZyI7czoyOiJoMyI7fWk6MTthOjI6e3M6NToidGl0bGUiO3M6MjY6Ildvb2NvbW1lcmNlIFNpbmdsZSBTaWRlYmFyIjtzOjIwOiJ3aWRnZXRfdGl0bGVfaGVhZGluZyI7czoyOiJoMyI7fX1zOjEyOiJnb29nbGVfZm9udHMiO2E6MTp7aTowO2E6MTp7czo2OiJmYW1pbHkiO3M6MDoiIjt9fXM6MjY6InM3dXBmX3NpZGViYXJfcG9zaXRpb25fd29vIjtzOjU6InJpZ2h0IjtzOjE3OiJzN3VwZl9zaWRlYmFyX3dvbyI7czoxOToid29vY29tbWVyY2Utc2lkZWJhciI7czoxNToic3Zfc2V0X3RpbWVfd29vIjtzOjA6IiI7czoxNToid29vX3Nob3BfbnVtYmVyIjtzOjI6IjEyIjtzOjE1OiJ3b29fc2hvcF9jb2x1bW4iO3M6MToiMyI7czozMDoic3Zfc2lkZWJhcl9wb3NpdGlvbl93b29fc2luZ2xlIjtzOjU6InJpZ2h0IjtzOjIxOiJzdl9zaWRlYmFyX3dvb19zaW5nbGUiO3M6MjY6Indvb2NvbW1lcmNlLXNpbmdsZS1zaWRlYmFyIjtzOjE1OiJhdHRyaWJ1dGVfc3R5bGUiO3M6Nzoic3BlY2lhbCI7czoxOToid29vX2F0dHJfYmFja2dyb3VuZCI7YTo5OntpOjA7YTozOntzOjU6InRpdGxlIjtzOjQ6IkJsdWUiO3M6OToiYXR0cl9zbHVnIjtzOjQ6ImJsdWUiO3M6NzoiYXR0cl9iZyI7czo3OiIjNGQ2ZGJkIjt9aToxO2E6Mzp7czo1OiJ0aXRsZSI7czo0OiJDeWFuIjtzOjk6ImF0dHJfc2x1ZyI7czo0OiJjeWFuIjtzOjc6ImF0dHJfYmciO3M6NzoiIzJmYmNkYSI7fWk6MjthOjM6e3M6NToidGl0bGUiO3M6NjoiT3JhbmdlIjtzOjk6ImF0dHJfc2x1ZyI7czo2OiJvcmFuZ2UiO3M6NzoiYXR0cl9iZyI7czo3OiIjZmJiNDUwIjt9aTozO2E6Mzp7czo1OiJ0aXRsZSI7czo1OiJHcmVlbiI7czo5OiJhdHRyX3NsdWciO3M6NToiZ3JlZW4iO3M6NzoiYXR0cl9iZyI7czo3OiIjNzJiMjI2Ijt9aTo0O2E6Mzp7czo1OiJ0aXRsZSI7czo0OiJHcmF5IjtzOjk6ImF0dHJfc2x1ZyI7czo0OiJncmF5IjtzOjc6ImF0dHJfYmciO3M6NzoiI2Q4ZDhkOSI7fWk6NTthOjM6e3M6NToidGl0bGUiO3M6NToiQmxhY2siO3M6OToiYXR0cl9zbHVnIjtzOjU6ImJsYWNrIjtzOjc6ImF0dHJfYmciO3M6NzoiIzIwMjAyMCI7fWk6NjthOjM6e3M6NToidGl0bGUiO3M6MzoiUmVkIjtzOjk6ImF0dHJfc2x1ZyI7czozOiJyZWQiO3M6NzoiYXR0cl9iZyI7czo3OiIjZmI1ZDVkIjt9aTo3O2E6Mzp7czo1OiJ0aXRsZSI7czo2OiJZZWxsb3ciO3M6OToiYXR0cl9zbHVnIjtzOjY6InllbGxvdyI7czo3OiJhdHRyX2JnIjtzOjc6IiNmZmUwMGMiO31pOjg7YTozOntzOjU6InRpdGxlIjtzOjU6IldoaXRlIjtzOjk6ImF0dHJfc2x1ZyI7czo1OiJ3aGl0ZSI7czo3OiJhdHRyX2JnIjtzOjc6IiNmZmZmZmYiO319czoxODoic2hvd19zaW5nbGVfbnVtYmVyIjtzOjA6IiI7czoxOToic2hvd19zaW5nbGVfbGFzdGVzdCI7czozOiJvZmYiO3M6MTg6InNob3dfc2luZ2xlX3Vwc2VsbCI7czoyOiJvbiI7czoxODoic2hvd19zaW5nbGVfcmVsYXRlIjtzOjI6Im9uIjt9';
        $s7upf_config['import_widget'] = '{"blog-sidebar":{"categories-2":{"title":"CATEGORIES","count":0,"hierarchical":0,"dropdown":0},"meta-2":{"title":""},"s7upf_listpostswidget-1":{"title":"RECENT POSTS","posts_per_page":"5","category":"0","order":"desc","order_by":"None"},"tag_cloud-1":{"title":"Tags","taxonomy":"post_tag"},"s7upf_instagram_widget-1":{"title":"Instagram Feed","username":"envato","number":"4","target":"_self"}},"woocommerce-sidebar":{"sv_category_fillter-2":{"title":"Product Categories","category":["bed-bath","decor","furniture","lighting"]},"woocommerce_price_filter-1":{"title":"Filter by price"},"s7upf_attribute_filter-1":{"title":"SHOP BY COLOR","attribute":"color"},"s7upf_attribute_filter-2":{"title":"SHOP BY Size","attribute":"size"},"s7upf_list_products-1":{"title":"TOP SELLERS","number":"3","product_type":"bestsell"}},"woocommerce-single-sidebar":{"woocommerce_product_categories-2":{"title":"Product Categories","orderby":"name","dropdown":0,"count":0,"hierarchical":1,"show_children_only":0,"hide_empty":0},"s7upf_list_products-2":{"title":"TOP SELLERS","number":"4","product_type":"bestsell"},"woocommerce_product_tag_cloud-2":{"title":"Product Tags"},"s7upf_list_products-3":{"title":"Featured Products","number":"3","product_type":"featured"}}}';
        $s7upf_config['import_category'] = '';

        /**************************************** PLUGINS ****************************************/

        $s7upf_config['require-plugin'] = array(    
            array(
                'name'               => esc_html__('Option Tree', 'lucky'), // The plugin name.
                'slug'               => 'option-tree', // The plugin slug (typically the folder name).
                'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            ),
            array(
                'name'      => esc_html__( 'Contact Form 7', 'lucky'),
                'slug'      => 'contact-form-7',
                'required'  => true,
            ),
            array(
                'name'      => esc_html__( 'Visual Composer', 'lucky'),
                'slug'      => 'js_composer',
                'required'  => true,
                'source'    =>get_template_directory_uri().'/plugins/js_composer.zip'
            ),
            array(
                'name'      => esc_html__( '7up Core', 'lucky'),
                'slug'      => '7up-core',
                'required'  => true,
                'source'    =>get_template_directory_uri().'/plugins/7up-core.zip'
            ),
            array(
                'name'      => esc_html__( 'WooCommerce', 'lucky'),
                'slug'      => 'woocommerce',
                'required'  => true,
            ),            
            array(
                'name'      => esc_html__('MailChimp for WordPress Lite','lucky'),
                'slug'      => 'mailchimp-for-wp',
                'required'  => true,
            ),
            array(
                'name'      => esc_html__('Yith Woocommerce Compare','lucky'),
                'slug'      => 'yith-woocommerce-compare',
                'required'  => true,
            ),
            array(
                'name'      => esc_html__('Yith Woocommerce Wishlist','lucky'),
                'slug'      => 'yith-woocommerce-wishlist',
                'required'  => true,
            )
        );

    /**************************************** PLUGINS ****************************************/
        $s7upf_config['theme-option'] = array(
            'sections' => array(
                array(
                    'id' => 'option_general',
                    'title' => '<i class="fa fa-cog"></i>'.esc_html__(' General Settings', 'lucky')
                ),
                array(
                    'id' => 'option_logo',
                    'title' => '<i class="fa fa-image"></i>'.esc_html__(' Logo Settings', 'lucky')
                ),
                array(
                    'id' => 'option_menu',
                    'title' => '<i class="fa fa-align-justify"></i>'.esc_html__(' Menu Settings', 'lucky')
                ),
                array(
                    'id' => 'option_layout',
                    'title' => '<i class="fa fa-columns"></i>'.esc_html__(' Layout Settings', 'lucky')
                ),
                array(
                    'id' => 'option_typography',
                    'title' => '<i class="fa fa-font"></i>'.esc_html__(' Typography', 'lucky')
                )
            ),
            'settings' => array(
                 /*----------------Begin General --------------------*/
                array(
                    'id'          => 's7upf_header_page',
                    'label'       => esc_html__( 'Header Page', 'lucky' ),
                    'desc'        => esc_html__( 'Include page to Header', 'lucky' ),
                    'type'        => 'select',
                    'section'     => 'option_general',
                    'choices'     => s7upf_list_header_page()
                ),
                array(
                    'id'          => 's7upf_footer_page',
                    'label'       => esc_html__( 'Footer Page', 'lucky' ),
                    'desc'        => esc_html__( 'Include page to Footer', 'lucky' ),
                    'type'        => 'page-select',
                    'section'     => 'option_general'
                ),
                array(
                    'id'          => 's7upf_404_page',
                    'label'       => esc_html__( '404 Page', 'lucky' ),
                    'desc'        => esc_html__( 'Include page to 404 page', 'lucky' ),
                    'type'        => 'page-select',
                    'section'     => 'option_general'
                ),
                array(
                    'id'          => 'show_header_page',
                    'label'       => esc_html__('Header page image','lucky'),
                    'type'        => 'on-off',
                    'section'     => 'option_general',
                    'std'         => 'off'
                ),
                array(
                    'id'          => 'header_page_image',
                    'label'       => esc_html__('Header page Image','lucky'),
                    'type'        => 'list-item',
                    'section'     => 'option_general',
                    'condition'   => 'show_header_page:is(on)',
                    'settings'    => array( 
                        array(
                            'id'          => 'header_image',
                            'label'       => esc_html__('Header image','lucky'),
                            'type'        => 'upload',
                        ),
                        array(
                            'id'          => 'header_link',
                            'label'       => esc_html__('Link','lucky'),
                            'type'        => 'text',                            
                        ),
                    ),
                ),
                array(
                    'id' => 'enable_rtl',
                    'label' => esc_html__('Enqueue RTL style', 'lucky'),
                    'type' => 'on-off',
                    'section' => 'option_general',
                    'std' => 'off'
                ),
                array(
                    'id' => 's7upf_show_breadrumb',
                    'label' => esc_html__('Show BreadCrumb', 'lucky'),
                    'desc' => esc_html__('This allow you to show or hide BreadCrumb', 'lucky'),
                    'type' => 'on-off',
                    'section' => 'option_general',
                    'std' => 'on'
                ),
                array(
                    'id'          => 's7upf_bg_breadcrumb',
                    'label'       => esc_html__('Background Breadcrumb','lucky'),
                    'type'        => 'background',
                    'section'     => 'option_general',
                    'condition'   => 's7upf_show_breadrumb:is(on)',
                ),                
                array(
                    'id' => 'show_scroll_top',
                    'label' => esc_html__('Show Scroll Top', 'lucky'),
                    'desc' => esc_html__('This allow you to show or hide Scroll top button', 'lucky'),
                    'type' => 'on-off',
                    'section' => 'option_general',
                    'std' => 'off'
                ),
                array(
                    'id'          => 'main_color',
                    'label'       => esc_html__('Main color','lucky'),
                    'type'        => 'colorpicker',
                    'section'     => 'option_general',
                ),                
                array(
                    'id'          => 'map_api_key',
                    'label'       => esc_html__('Map API key','lucky'),
                    'type'        => 'text',
                    'section'     => 'option_general',
                    'std'         => 'AIzaSyBX2IiEBg-0lQKQQ6wk6sWRGQnWI7iogf0',
                ),
                array(
                    'id'          => 'custom_css',
                    'label'       => esc_html__('Custom CSS','lucky'),
                    'type'        => 'textarea-simple',
                    'section'     => 'option_general',
                ),
                /*----------------End General ----------------------*/

                /*----------------Begin Logo --------------------*/
                array(
                    'id' => 'logo',
                    'label' => esc_html__('Logo', 'lucky'),
                    'desc' => esc_html__('This allow you to change logo', 'lucky'),
                    'type' => 'upload',
                    'section' => 'option_logo',
                ),        
                array(
                    'id' => 'favicon',
                    'label' => esc_html__('Favicon', 'lucky'),
                    'desc' => esc_html__('This allow you to change favicon of your website', 'lucky'),
                    'type' => 'upload',
                    'section' => 'option_logo'
                ),
                /*----------------End Logo ----------------------*/

                /*----------------Begin Menu --------------------*/
                array(
                    'id'          => 's7upf_menu_fixed',
                    'label'       => esc_html__('Menu Fixed','lucky'),
                    'desc'        => 'Menu change to fixed when scroll',
                    'type'        => 'on-off',
                    'section'     => 'option_menu',
                    'std'         => 'on',
                ),
                array(
                    'id'          => 's7upf_menu_color',
                    'label'       => esc_html__('Menu style','lucky'),
                    'type'        => 'typography',
                    'section'     => 'option_menu',
                ),
                array(
                    'id'          => 's7upf_menu_color_hover',
                    'label'       => esc_html__('Hover color','lucky'),
                    'desc'        => esc_html__('Choose color','lucky'),
                    'type'        => 'colorpicker',
                    'section'     => 'option_menu',
                ),
                array(
                    'id'          => 's7upf_menu_color_active',
                    'label'       => esc_html__('Background hover color','lucky'),
                    'desc'        => esc_html__('Choose color','lucky'),
                    'type'        => 'colorpicker',
                    'section'     => 'option_menu',
                ),
                array(
                    'id'          => 's7upf_menu_color2',
                    'label'       => esc_html__('Menu sub style','lucky'),
                    'type'        => 'typography',
                    'section'     => 'option_menu',
                ),
                array(
                    'id'          => 's7upf_menu_color_hover2',
                    'label'       => esc_html__('Hover sub color','lucky'),
                    'desc'        => esc_html__('Choose color','lucky'),
                    'type'        => 'colorpicker',
                    'section'     => 'option_menu',
                ),
                array(
                    'id'          => 's7upf_menu_color_active2',
                    'label'       => esc_html__('Background hover sub color','lucky'),
                    'desc'        => esc_html__('Choose color','lucky'),
                    'type'        => 'colorpicker',
                    'section'     => 'option_menu',
                ),
                /*----------------End Menu ----------------------*/
                

                /*----------------Begin Layout --------------------*/
                array(
                    'id'          => 's7upf_sidebar_position_blog',
                    'label'       => esc_html__('Sidebar Blog','lucky'),
                    'type'        => 'select',
                    'section'     => 'option_layout',
                    'desc'=>esc_html__('Left, or Right, or Center','lucky'),
                    'choices'     => array(
                        array(
                            'value'=>'no',
                            'label'=>esc_html__('No Sidebar','lucky'),
                        ),
                        array(
                            'value'=>'left',
                            'label'=>esc_html__('Left','lucky'),
                        ),
                        array(
                            'value'=>'right',
                            'label'=>esc_html__('Right','lucky'),
                        )
                    )
                ),
                array(
                    'id'          => 's7upf_sidebar_blog',
                    'label'       => esc_html__('Sidebar select display in blog','lucky'),
                    'type'        => 'sidebar-select',
                    'section'     => 'option_layout',
                    'condition'   => 's7upf_sidebar_position_blog:not(no)',
                ),
                /****end blog****/
                array(
                    'id'          => 's7upf_sidebar_position_page',
                    'label'       => esc_html__('Sidebar Page','lucky'),
                    'type'        => 'select',
                    'section'     => 'option_layout',
                    'desc'=>esc_html__('Left, or Right, or Center','lucky'),
                    'choices'     => array(
                        array(
                            'value'=>'no',
                            'label'=>esc_html__('No Sidebar','lucky'),
                        ),
                        array(
                            'value'=>'left',
                            'label'=>esc_html__('Left','lucky'),
                        ),
                        array(
                            'value'=>'right',
                            'label'=>esc_html__('Right','lucky'),
                        )
                    )
                ),
                array(
                    'id'          => 's7upf_sidebar_page',
                    'label'       => esc_html__('Sidebar select display in page','lucky'),
                    'type'        => 'sidebar-select',
                    'section'     => 'option_layout',
                    'condition'   => 's7upf_sidebar_position_page:not(no)',
                ),
                /****end page****/
                array(
                    'id'          => 's7upf_sidebar_position_page_archive',
                    'label'       => esc_html__('Sidebar Position on Page Archives:','lucky'),
                    'type'        => 'select',
                    'section'     => 'option_layout',
                    'desc'=>esc_html__('Left, or Right, or Center','lucky'),
                    'choices'     => array(
                        array(
                            'value'=>'no',
                            'label'=>esc_html__('No Sidebar','lucky'),
                        ),
                        array(
                            'value'=>'left',
                            'label'=>esc_html__('Left','lucky'),
                        ),
                        array(
                            'value'=>'right',
                            'label'=>esc_html__('Right','lucky'),
                        )
                    )
                ),
                array(
                    'id'          => 's7upf_sidebar_page_archive',
                    'label'       => esc_html__('Sidebar select display in page Archives','lucky'),
                    'type'        => 'sidebar-select',
                    'section'     => 'option_layout',
                    'condition'   => 's7upf_sidebar_position_page_archive:not(no)',
                ),
                // END
                array(
                    'id'          => 's7upf_sidebar_position_post',
                    'label'       => esc_html__('Sidebar Single Post','lucky'),
                    'type'        => 'select',
                    'section'     => 'option_layout',
                    'desc'=>esc_html__('Left, or Right, or Center','lucky'),
                    'choices'     => array(
                        array(
                            'value'=>'no',
                            'label'=>esc_html__('No Sidebar','lucky'),
                        ),
                        array(
                            'value'=>'left',
                            'label'=>esc_html__('Left','lucky'),
                        ),
                        array(
                            'value'=>'right',
                            'label'=>esc_html__('Right','lucky'),
                        )
                    )
                ),
                array(
                    'id'          => 's7upf_sidebar_post',
                    'label'       => esc_html__('Sidebar select display in single post','lucky'),
                    'type'        => 'sidebar-select',
                    'section'     => 'option_layout',
                    'condition'   => 's7upf_sidebar_position_post:not(no)',
                ),
                array(
                    'id'          => 's7upf_add_sidebar',
                    'label'       => esc_html__('Add SideBar','lucky'),
                    'type'        => 'list-item',
                    'section'     => 'option_layout',
                    'std'         => '',
                    'settings'    => array( 
                        array(
                            'id'          => 'widget_title_heading',
                            'label'       => esc_html__('Choose heading title widget','lucky'),
                            'type'        => 'select',
                            'std'        => 'h3',
                            'choices'     => array(
                                array(
                                    'value'=>'h1',
                                    'label'=>esc_html__('H1','lucky'),
                                ),
                                array(
                                    'value'=>'h2',
                                    'label'=>esc_html__('H2','lucky'),
                                ),
                                array(
                                    'value'=>'h3',
                                    'label'=>esc_html__('H3','lucky'),
                                ),
                                array(
                                    'value'=>'h4',
                                    'label'=>esc_html__('H4','lucky'),
                                ),
                                array(
                                    'value'=>'h5',
                                    'label'=>esc_html__('H5','lucky'),
                                ),
                                array(
                                    'value'=>'h6',
                                    'label'=>esc_html__('H6','lucky'),
                                ),
                            )
                        ),
                    ),
                ),
                /*----------------End Layout ----------------------*/

                /*----------------Begin Blog ----------------------*/       
                

                /*----------------End BLOG----------------------*/

                /*----------------Begin Typography ----------------------*/
                array(
                    'id'          => 's7upf_custom_typography',
                    'label'       => esc_html__('Add Settings','lucky'),
                    'type'        => 'list-item',
                    'section'     => 'option_typography',
                    'std'         => '',
                    'settings'    => array(
                        array(
                            'id'          => 'typo_area',
                            'label'       => esc_html__('Choose Area to style','lucky'),
                            'type'        => 'select',
                            'std'        => 'main',
                            'choices'     => array(
                                array(
                                    'value'=>'header',
                                    'label'=>esc_html__('Header','lucky'),
                                ),
                                array(
                                    'value'=>'main',
                                    'label'=>esc_html__('Main Content','lucky'),
                                ),
                                array(
                                    'value'=>'widget',
                                    'label'=>esc_html__('Widget','lucky'),
                                ),
                                array(
                                    'value'=>'footer',
                                    'label'=>esc_html__('Footer','lucky'),
                                ),
                            )
                        ),
                        array(
                            'id'          => 'typo_heading',
                            'label'       => esc_html__('Choose heading Area','lucky'),
                            'type'        => 'select',
                            'std'        => 'h3',
                            'choices'     => array(
                                array(
                                    'value'=>'h1',
                                    'label'=>esc_html__('H1','lucky'),
                                ),
                                array(
                                    'value'=>'h2',
                                    'label'=>esc_html__('H2','lucky'),
                                ),
                                array(
                                    'value'=>'h3',
                                    'label'=>esc_html__('H3','lucky'),
                                ),
                                array(
                                    'value'=>'h4',
                                    'label'=>esc_html__('H4','lucky'),
                                ),
                                array(
                                    'value'=>'h5',
                                    'label'=>esc_html__('H5','lucky'),
                                ),
                                array(
                                    'value'=>'h6',
                                    'label'=>esc_html__('H6','lucky'),
                                ),
                                array(
                                    'value'=>'a',
                                    'label'=>esc_html__('a','lucky'),
                                ),
                                array(
                                    'value'=>'p',
                                    'label'=>esc_html__('p','lucky'),
                                ),
                            )
                        ),
                        array(
                            'id'          => 'typography_style',
                            'label'       => esc_html__('Add Style','lucky'),
                            'type'        => 'typography',
                            'section'     => 'option_typography',
                        ),
                    ),
                ),        
                array(
                    'id'          => 'google_fonts',
                    'label'       => esc_html__('Add Google Fonts','lucky'),
                    'type'        => 'google-fonts',
                    'section'     => 'option_typography',
                ),
                /*----------------End Typography ----------------------*/
            )
        );
        if(class_exists( 'WooCommerce' )){
            array_push($s7upf_config['theme-option']['sections'], array(
                                                        'id' => 'option_woo',
                                                        'title' => '<i class="fa fa-shopping-cart"></i>'.esc_html__(' Shop Settings', 'lucky')
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 's7upf_sidebar_position_woo',
                                                        'label'       => esc_html__('Sidebar Position WooCommerce page','lucky'),
                                                        'type'        => 'select',
                                                        'section'     => 'option_woo',
                                                        'desc'=>esc_html__('Left, or Right, or Center','lucky'),
                                                        'choices'     => array(
                                                            array(
                                                                'value'=>'no',
                                                                'label'=>esc_html__('No Sidebar','lucky'),
                                                            ),
                                                            array(
                                                                'value'=>'left',
                                                                'label'=>esc_html__('Left','lucky'),
                                                            ),
                                                            array(
                                                                'value'=>'right',
                                                                'label'=>esc_html__('Right','lucky'),
                                                            )
                                                        )
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 's7upf_sidebar_woo',
                                                        'label'       => esc_html__('Sidebar select WooCommerce page','lucky'),
                                                        'type'        => 'sidebar-select',
                                                        'section'     => 'option_woo',
                                                        'condition'   => 's7upf_sidebar_position_woo:not(no)',
                                                        'desc'        => esc_html__('Choose one style of sidebar for WooCommerce page','lucky'),

                                                    ));            
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'sv_set_time_woo',
                                                        'label'       => esc_html__('Product new in(days)','lucky'),
                                                        'type'        => 'text',
                                                        'section'     => 'option_woo',
                                                        'desc'        => esc_html__('Enter number to set time for product is new. Unit day. Default is 30.','lucky')
                                                    ));
            
             array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'shop_ajax',
                                                        'label'       => esc_html__('Shop ajax','lucky'),
                                                        'type'        => 'on-off',
                                                        'section'     => 'option_woo',
                                                        'std'         => 'off'
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'woo_shop_number',
                                                        'label'       => esc_html__('Product Number','lucky'),
                                                        'type'        => 'text',
                                                        'section'     => 'option_woo',
                                                        'desc'        => esc_html__('Enter number product to display per page. Default is 12.','lucky')
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'woo_shop_column',
                                                        'label'       => esc_html__('Choose shop column','lucky'),
                                                        'type'        => 'select',
                                                        'section'     => 'option_woo',
                                                        'choices'     => array(
                                                            array(
                                                                'value'=> 2,
                                                                'label'=> 2,
                                                            ),
                                                            array(
                                                                'value'=> 3,
                                                                'label'=> 3,
                                                            ),
                                                            array(
                                                                'value'=> 4,
                                                                'label'=> 4,
                                                            ),
                                                        )
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'shop_custom_size_thumb',
                                                        'label'       => esc_html__('Shop grid custom thumbnail size','lucky'),
                                                        'type'        => 'text',
                                                        'section'     => 'option_woo',
                                                        'desc'        => esc_html__('Enter product thumbnail size format [width]x[height]. Example 200x300. If empty is auto crop.','lucky')
                                                    ));
            array_push($s7upf_config['theme-option']['sections'], array(
                                                        'id' => 'option_product',
                                                        'title' => '<i class="fa fa-th-large"></i>'.esc_html__(' Product Settings', 'lucky')
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'sv_sidebar_position_woo_single',
                                                        'label'       => esc_html__('Sidebar Position WooCommerce Single','lucky'),
                                                        'type'        => 'select',
                                                        'section'     => 'option_product',
                                                        'desc'=>esc_html__('Left, or Right, or Center','lucky'),
                                                        'std'         => 'no',
                                                        'choices'     => array(
                                                            array(
                                                                'value'=>'no',
                                                                'label'=>esc_html__('No Sidebar','lucky'),
                                                            ),
                                                            array(
                                                                'value'=>'left',
                                                                'label'=>esc_html__('Left','lucky'),
                                                            ),
                                                            array(
                                                                'value'=>'right',
                                                                'label'=>esc_html__('Right','lucky'),
                                                            ),
                                                        )
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'sv_sidebar_woo_single',
                                                        'label'       => esc_html__('Sidebar select WooCommerce Single','lucky'),
                                                        'type'        => 'sidebar-select',
                                                        'section'     => 'option_product',
                                                        'condition'   => 'sv_sidebar_position_woo_single:not(no)',
                                                        'desc'        => esc_html__('Choose one style of sidebar for WooCommerce page','lucky'),
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'attribute_style',
                                                        'label'       => esc_html__('Attribute Style','lucky'),
                                                        'type'        => 'select',
                                                        'section'     => 'option_product',
                                                        'choices'     => array(                                                    
                                                            array(
                                                                'value'=> 'normal',
                                                                'label'=> esc_html__("Normal", 'lucky'),
                                                            ),
                                                            array(
                                                                'value'=> 'special',
                                                                'label'=> esc_html__("Special", 'lucky'),
                                                            ),
                                                        )
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'woo_attr_background',
                                                        'label'       => esc_html__('Product Attribute Background','lucky'),
                                                        'type'        => 'list-item',
                                                        'section'     => 'option_product',
                                                        'std'         => '',
                                                        'settings'    => array( 
                                                            array(
                                                                'id'          => 'attr_slug',
                                                                'label'       => esc_html__('Term Slug Attribute','lucky'),
                                                                'type'        => 'text',
                                                            ),
                                                            array(
                                                                'id'          => 'attr_bg',
                                                                'label'       => esc_html__('Term Attribute Background','lucky'),
                                                                'type'        => 'colorpicker',
                                                            ),
                                                        ),
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'show_single_number',
                                                        'label'       => esc_html__('Show Single Products Number','lucky'),
                                                        'type'        => 'text',
                                                        'section'     => 'option_product',
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'show_single_lastest',
                                                        'label'       => esc_html__('Show Single Lastest Products','lucky'),
                                                        'type'        => 'on-off',
                                                        'section'     => 'option_product',
                                                        'std'         => 'off'
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'show_single_upsell',
                                                        'label'       => esc_html__('Show Single Upsell Products','lucky'),
                                                        'type'        => 'on-off',
                                                        'section'     => 'option_product',
                                                        'std'         => 'on'
                                                    ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                        'id'          => 'show_single_relate',
                                                        'label'       => esc_html__('Show Single Relate Products','lucky'),
                                                        'type'        => 'on-off',
                                                        'section'     => 'option_product',
                                                        'std'         => 'off'
                                                    ));            
            array_push($s7upf_config['theme-option']['sections'], array(
                                                                'id' => 'option_catelog',
                                                                'title' => '<i class="fa fa-shopping-cart"></i>'.esc_html__(' WooCommerce Catalog', 'lucky')
                                                            ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                                'id'          => 'woo_catelog',
                                                                'label'       => esc_html__('Enable WooCommerce Catalog Mode','lucky'),
                                                                'type'        => 'on-off',
                                                                'section'     => 'option_catelog',
                                                                'std'         => 'off'
                                                            ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                                'id'          => 'hide_detail',
                                                                'label'       => esc_html__('Hide "Add to cart" button in product detail page','lucky'),
                                                                'type'        => 'on-off',
                                                                'section'     => 'option_catelog',
                                                                'condition'   => 'woo_catelog:is(on)',
                                                                'std'         => 'off'
                                                            ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                                'id'          => 'hide_other_page',
                                                                'label'       => esc_html__('Hide "Add to cart" button in other shop pages','lucky'),
                                                                'type'        => 'on-off',
                                                                'section'     => 'option_catelog',
                                                                'condition'   => 'woo_catelog:is(on)',
                                                                'std'         => 'off',
                                                            ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                                'id'          => 'hide_admin',
                                                                'label'       => esc_html__('Enable Catalog Mode also for administrators','lucky'),
                                                                'type'        => 'on-off',
                                                                'section'     => 'option_catelog',
                                                                'condition'   => 'woo_catelog:is(on)',
                                                                'std'         => 'off',
                                                            ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                                'id'          => 'hide_price',
                                                                'label'       => esc_html__('Hide Price','lucky'),
                                                                'type'        => 'on-off',
                                                                'section'     => 'option_catelog',
                                                                'condition'   => 'woo_catelog:is(on)',
                                                                'std'         => 'off',
                                                            ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                                'id'          => 'hide_minicart',
                                                                'label'       => esc_html__('Hide Mini Cart','lucky'),
                                                                'type'        => 'on-off',
                                                                'section'     => 'option_catelog',
                                                                'condition'   => 'woo_catelog:is(on)',
                                                                'std'         => 'off',
                                                            ));
            array_push($s7upf_config['theme-option']['settings'],array(
                                                                'id'          => 'hide_shop',
                                                                'label'       => esc_html__('Disable shop','lucky'),
                                                                'type'        => 'on-off',
                                                                'section'     => 'option_catelog',
                                                                'condition'   => 'woo_catelog:is(on)',
                                                                'std'         => 'off',
                                                                'desc'        => esc_html__('Hide and disable "Cart" page, "Checkout" page and all "Add to Cart" buttons','lucky')
                                                            ));
        }
    }
}
s7upf_set_theme_config();