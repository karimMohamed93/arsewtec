<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 31/08/15
 * Time: 10:00 AM
 */
/************************************Main Carousel*************************************/
if(!function_exists('s7upf_vc_slide_slick'))
{
    function s7upf_vc_slide_slick($attr, $content = false)
    {
        $html = $css_class = '';
        extract(shortcode_atts(array(
            'custom_css' => '',
        ),$attr));
        if(!empty($custom_css)) $css_class = vc_shortcode_custom_css_class( $custom_css );
            $html .=    '<div class="banner-slider banner-slider7">
                            <div class="slick center '.esc_attr($css_class).'">';
            $html .=            wpb_js_remove_wpautop($content, false);
            $html .=        '</div>';
            $html .=    '</div>';
        return $html;
    }
}
stp_reg_shortcode('slide_slick','s7upf_vc_slide_slick');
vc_map(
    array(
        'name'     => esc_html__( 'Slick Slider', 'lucky' ),
        'base'     => 'slide_slick',
        'category' => esc_html__( '7Up-theme', 'lucky' ),
        'icon'     => 'icon-st',
        'as_parent' => array( 'only' => 'vc_column_text,vc_single_image,slide_slick_item' ),
        'content_element' => true,
        'js_view' => 'VcColumnView',
        'params'   => array(
            array(
                "type"          => "css_editor",
                "heading"       => esc_html__("Custom Block",'lucky'),
                "param_name"    => "custom_css",
            )
        )
    )
);

/*******************************************END MAIN*****************************************/


/**************************************BEGIN ITEM************************************/
//Banner item Frontend
if(!function_exists('s7upf_vc_slide_slick_item'))
{
    function s7upf_vc_slide_slick_item($attr, $content = false)
    {
        $html = $view_html = $view_html2 = '';
        extract(shortcode_atts(array(
            'image'         => '',
            'link'          => '',            
            'thumb_animation' => '',
            'info_animation' => '',
            'info_style' => 'black',
        ),$attr));
        if(!empty($image)){
            $thumb_class = $info_class = '';
            if(!empty($thumb_animation)) $thumb_class = 'animated';
            if(!empty($info_animation)) $info_class = 'animated';
            $html .=    '<div class="item-slider">
                            <div class="banner-thumb '.esc_attr($thumb_class).'" data-anim-type="'.esc_attr($thumb_animation).'">
                                <a href="'.esc_url($link).'">'.wp_get_attachment_image($image,'full').'</a>
                            </div>
                            <div class="banner-info text-uppercase text-center '.esc_attr($info_style).' '.esc_attr($info_class).'" data-anim-type="'.esc_attr($info_animation).'">
                                '.wpb_js_remove_wpautop($content, true).'
                            </div>
                        </div>';
        }
        return $html;
    }
}
stp_reg_shortcode('slide_slick_item','s7upf_vc_slide_slick_item');

// Banner item
vc_map(
    array(
        'name'     => esc_html__( 'Slick Item', 'lucky' ),
        'base'     => 'slide_slick_item',
        'icon'     => 'icon-st',
        'content_element' => true,
        'as_child' => array('only' => 'slide_slick'),
        'params'   => array(                     
            array(
                'type'        => 'attach_image',
                'heading'     => esc_html__( 'Image', 'lucky' ),
                'param_name'  => 'image',
            ),
            array(
                'type'        => 'textfield',
                'heading'     => esc_html__( 'Link Banner', 'lucky' ),
                'param_name'  => 'link',
            ),            
            array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Image Animation', 'lucky' ),
                    'param_name' => 'thumb_animation',
                    'value' => s7upf_get_list_animation()
                ),
            array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Info Animation', 'lucky' ),
                    'param_name' => 'info_animation',
                    'value' => s7upf_get_list_animation()
                ),
            array(
                    'type' => 'dropdown',
                    'heading' => esc_html__( 'Info Style', 'lucky' ),
                    'param_name' => 'info_style',
                    'value' => array(
                        esc_html__( 'Black', 'lucky' )     => 'black',
                        esc_html__( 'White', 'lucky' )     => 'white',
                        )
                ),
            array(
                "type" => "textarea_html",
                "holder" => "div",
                "heading" => esc_html__("Content",'lucky'),
                "param_name" => "content",
            ),
        )
    )
);

/**************************************END ITEM************************************/


//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Slide_Slick extends WPBakeryShortCodesContainer {}
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Slide_Slick_Item extends WPBakeryShortCode {}
}