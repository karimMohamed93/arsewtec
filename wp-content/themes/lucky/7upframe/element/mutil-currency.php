<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 12/08/15
 * Time: 10:00 AM
 */
if(class_exists('Woo_Multi_Currency')){
	if(!function_exists('s7upf_vc_selector_currency'))
	{
	    function s7upf_vc_selector_currency($attr)
	    {
	        $html = $list_currency = '';
	        extract(shortcode_atts(array(
	            'style'      => '',
	            'symbol'     => '',
	            'name'       => '',
	        ),$attr));
	        $selected_currencies = get_option( 'wmc_selected_currencies' );
	 		$woocommerce_currency = get_option( 'woocommerce_currency' );
	 		if(isset($_COOKIE['wmc_current_currency'])) $woocommerce_currency = $_COOKIE['wmc_current_currency'];
	 		$woocommerce_currencies = get_woocommerce_currencies();
	 		if(!empty($selected_currencies)){
	 			foreach ($selected_currencies as $key => $value) {
	 				$list_currency .= '<li><a href="'.esc_url(str_replace('&', '&amp;',add_query_arg( 'wmc_current_currency', $key ))).'">'.$value['symbol'].' '.$woocommerce_currencies[$key].'</a></li>';
	 			}
	 		}
	 		switch ($style) {
	 			case 'select':
	 				$html .=	do_shortcode("[woo_multi_currency]");
	 				break;
	 			
	 			default:
	 				$html .=	'<a class="currency-link" href="'.esc_url(str_replace('&', '&amp;',add_query_arg( 'wmc_current_currency', $woocommerce_currency ))).'">'.$selected_currencies[$woocommerce_currency]['symbol'].' '.$woocommerce_currencies[$woocommerce_currency].'</a>
								<ul>
									'.$list_currency.'
								</ul>';
	 				break;
	 		}
	        return $html;
	    }
	}

	stp_reg_shortcode('s7upf_selector_currency','s7upf_vc_selector_currency');

	vc_map( array(
	    "name"      => esc_html__("SV Selector Currency", 'lucky'),
	    "base"      => "s7upf_selector_currency",
	    "icon"      => "icon-st",
	    "category"  => '7Up-theme',
	    "params"    => array(
	        array(
	            "type" => "dropdown",
	            "holder" => "div",
	            "heading" => esc_html__("Style",'lucky'),
	            "param_name" => "style",
	            "value"		=> array(
	            	esc_html__("Default",'lucky')		=> '',
	            	esc_html__("Select",'lucky')		=> 'select',
	            	)            
	        )
	    )
	));
}