<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 05/09/15
 * Time: 10:00 AM
 */
if(class_exists("woocommerce")){
    if(!function_exists('s7upf_vc_product_tab'))
    {
        function s7upf_vc_product_tab($attr, $content = false)
        {
            $html = $el_class = $html_wl = $html_cp = '';
            extract(shortcode_atts(array(
                'style'             => '',
                'load_more'         => 'show',
                'filter'            => '',
                'tabs'              => '',
                'number'            => '',
                'cats'              => '',
                'order_by'          => 'date',
                'order'             => 'DESC',
                'item_style'        => 'list-product1',
            ),$attr));
            $data_load = array(
                'number'  => $number,
                'cats'  => $cats,
                'order'  => $order,
                'order_by'  => $order_by,
                'load_more'  => $load_more,
                'item_style'  => $item_style,
                );
            $args=array(
                'post_type'         => 'product',
                'posts_per_page'    => $number,
                'orderby'           => $order_by,
                'order'             => $order
            );
            if(!empty($cats)) {
                $custom_list = explode(",",$cats);
                $args['tax_query'][]=array(
                    'taxonomy'=>'product_cat',
                    'field'=>'slug',
                    'terms'=> $custom_list
                );
            }
            $pre = rand(1,100);
            if(!empty($tabs)){
                $tabs = explode(',', $tabs);
                $tab_html = $content_html = '';
                foreach ($tabs as $key => $tab) {
                    switch ($tab) {
                        case 'bestsell':
                            $tab_title =    esc_html__("Popular","lucky");
                            $args['meta_key'] = 'total_sales';
                            $args['orderby'] = 'meta_value_num';
                            break;

                        case 'toprate':
                            $tab_title =    esc_html__("Most review","lucky");
                            unset($args['meta_key']);
                            $args['meta_key'] = '_wc_average_rating';
                            $args['orderby'] = 'meta_value_num';
                            $args['meta_query'] = WC()->query->get_meta_query();
                            $args['tax_query'][] = WC()->query->get_tax_query();
                            break;
                        
                        case 'mostview':
                            $tab_title =    esc_html__("Most View","lucky");
                            unset($args['no_found_rows']);
                            unset($args['meta_query']);
                            unset($args['tax_query']);
                            if(!empty($cats)) {
                                $custom_list = explode(",",$cats);
                                $args['tax_query'][]=array(
                                    'taxonomy'=>'product_cat',
                                    'field'=>'slug',
                                    'terms'=> $custom_list
                                );
                            }
                            $args['meta_key'] = 'post_views';
                            $args['orderby'] = 'meta_value_num';
                            break;

                        case 'featured':
                            $tab_title =    esc_html__("Featured","lucky");
                            $args['orderby'] = $order_by;
                            $args['meta_key'] = '_featured';
                            $args['meta_value'] = 'yes';
                            break;

                        case 'trendding':
                            unset($args['meta_key']);
                            unset($args['meta_value']);
                            $tab_title =    esc_html__("Trending","lucky");
                            $args['meta_query'][] = array(
                                'key'     => 'trending_product',
                                'value'   => 'on',
                                'compare' => '=',
                            );
                            break;
                        
                        case 'onsale':
                            $tab_title =    esc_html__("On sale","lucky");
                            unset($args['meta_query']);
                            unset($args['meta_key']);
                            unset($args['meta_value']);
                            $args['meta_query']['relation']= 'OR';
                            $args['meta_query'][]=array(
                                'key'   => '_sale_price',
                                'value' => 0,
                                'compare' => '>',                
                                'type'          => 'numeric'
                            );
                            $args['meta_query'][]=array(
                                'key'   => '_min_variation_sale_price',
                                'value' => 0,
                                'compare' => '>',                
                                'type'          => 'numeric'
                            );
                            break;
                        
                        default:
                            $tab_title =    esc_html__("New arrivals","lucky");
                            $args['orderby'] = 'date';
                            break;
                    }
                    if($key == 0) $f_class = 'active';
                    else $f_class = '';
                    // $s_query = http_build_query($args);
                    $product_query = new WP_Query($args);
                    $count = 1;
                    $count_query = $product_query->post_count;
                    $max_page = $product_query->max_num_pages;
                    $data_load['product_type'] = $tab;
                    $data_loadjs = json_encode($data_load);
                    $tab_html .=    '<li class="'.$f_class.'"><a class="filter-ajax" href="'.esc_url('#'.$pre.$tab).'" data-toggle="tab" data-tab="'.esc_attr($tab).'">'.$tab_title.'</a></li>';
                    $content_html .=    '<div id="'.$pre.$tab.'" class="tab-pane content-load-wrap clearfix '.$f_class.'">
                                            <div class="content-style-list '.esc_attr($item_style).'">
                                                <div class="row content-load-ajax">';
                    if($product_query->have_posts()) {
                        while($product_query->have_posts()) {
                            $product_query->the_post();
                            global $product;
                            switch ($item_style) {
                                case 'list-product4':
                                    $check = rand(0,10);
                                    if($check % 2 == 0) $size = array(270,236);
                                    else $size = array(270,335);
                                    $content_html .=    '<div class="item-product-masonry">
                                                            <div class="product-thumb">
                                                                <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                                <div class="product-info">
                                                                    '.s7upf_product_link().'
                                                                    '.s7upf_get_rating_html().'
                                                                    <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                    '.s7upf_get_price_html().'
                                                                </div>
                                                            </div>
                                                        </div>';
                                    break;
                                
                                default:
                                    $content_html .=    '<div class="col-md-3 col-sm-4 col-xs-6">
                                                            <div class="item-product">
                                                                <div class="product-thumb">
                                                                    <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
                                                                    '.s7upf_product_link().'
                                                                    <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                                    '.s7upf_get_rating_html().'
                                                                    '.s7upf_get_saleoff_html().'
                                                                </div>
                                                                <div class="product-info">
                                                                    <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                                    '.s7upf_get_price_html().'
                                                                </div>
                                                            </div>
                                                        </div>';
                                    break;
                            }                            
                            $count++;
                        }
                    }
                    $content_html .=            '</div>
                                            </div>';
                    if($max_page > 1 && $load_more == 'show'){
                        $content_html .=         '<a href="#" class="load-ajax-btn btn-product-loadmore border radius" data-page="1" data-max_page="'.$max_page.'" data-load_data='."'".$data_loadjs.' '."'".'>'.esc_html__("Load more","lucky").'<i class="fa fa-spinner hidden" aria-hidden="true"></i></a>';
                    }
                    $content_html .=        '<input class="hidden load-ajax-btn" data-load_data='."'".$data_loadjs.' '."'".'>
                                        </div>';
                }
                
                $html .=    '<div class="product-tab-loadmore ajax-loadmore-'.esc_attr($load_more).' filter-'.esc_attr($filter).'">
                                <div class="title-tab1 text-center clearfix">
                                    <ul class="list-none">
                                        '.$tab_html.'
                                    </ul>';
                if($filter == 'show'){
                    $html .=        '<div class="filter-product pull-right">
                                        <a href="#" class="btn-filter">'.esc_html__("Filter","lucky").'</a>
                                        '.s7upf_product_filter_attr().'
                                    </div>';
                }
                $html .=        '</div>
                                <div class="tab-content">
                                    '.$content_html.'
                                </div>
                            </div>';
            }
            wp_reset_postdata();
            return $html;
        }
    }

    stp_reg_shortcode('sv_product_tab','s7upf_vc_product_tab');
    add_action( 'vc_before_init_base','s7upf_add_product_tab',10,100 );
    if ( ! function_exists( 's7upf_add_product_tab' ) ) {
        function s7upf_add_product_tab(){
            vc_map( array(
                "name"      => esc_html__("SV Product Tab", 'lucky'),
                "base"      => "sv_product_tab",
                "icon"      => "icon-st",
                "category"  => '7Up-theme',
                "params"    => array(
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__("Style",'lucky'),
                        "param_name" => "style",
                        "value"     => array(
                            esc_html__("Default",'lucky')   => '',
                            )
                    ),
                    array(
                        'heading'     => esc_html__( 'Number', 'lucky' ),
                        'type'        => 'textfield',
                        'description' => esc_html__( 'Enter number of product. Default is 10.', 'lucky' ),
                        'param_name'  => 'number',
                    ),
                    array(
                        'holder'     => 'div',
                        'heading'     => esc_html__( 'Product Categories', 'lucky' ),
                        'type'        => 'checkbox',
                        'param_name'  => 'cats',
                        'value'       => s7upf_list_taxonomy('product_cat',false)
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Order By', 'lucky' ),
                        'value' => s7upf_get_order_list(),
                        'param_name' => 'orderby',
                        'description' => esc_html__( 'Select Orderby Type ', 'lucky' ),
                        'edit_field_class'=>'vc_col-sm-6 vc_column',
                    ),
                    array(
                        'heading'     => esc_html__( 'Order', 'lucky' ),
                        'type'        => 'dropdown',
                        'param_name'  => 'order',
                        'value' => array(                   
                            esc_html__('Desc','lucky')  => 'DESC',
                            esc_html__('Asc','lucky')  => 'ASC',
                        ),
                        'description' => esc_html__( 'Select Order Type ', 'lucky' ),
                        'edit_field_class'=>'vc_col-sm-6 vc_column',
                    ),
                    array(
                        "type" => "checkbox",
                        "heading" => esc_html__("Tabs",'lucky'),
                        "param_name" => "tabs",
                        "value" => array(
                            esc_html__("New Arrivals",'lucky')    => 'newarrival',
                            esc_html__("Best Seller",'lucky')     => 'bestsell',
                            esc_html__("Most Review",'lucky')     => 'toprate',
                            esc_html__("Most View",'lucky')       => 'mostview',
                            esc_html__("Featured",'lucky')        => 'featured',
                            esc_html__("Trendding",'lucky')       => 'trendding',
                            esc_html__("On Sale",'lucky')         => 'onsale',
                            ),
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__("Item style",'lucky'),
                        "param_name" => "item_style",
                        "value"     => array(
                            esc_html__("Box shadow",'lucky')   => 'list-product1',
                            esc_html__("Full",'lucky')   => 'list-product2',
                            esc_html__("Masonry",'lucky')   => 'list-product4',
                            )
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__("Load more",'lucky'),
                        "param_name" => "load_more",
                        "value"     => array(
                            esc_html__("Show",'lucky')   => 'show',
                            esc_html__("Hidden",'lucky')   => 'hidden',
                            )
                    ),
                    array(
                        "type" => "dropdown",
                        "heading" => esc_html__("Filter Box",'lucky'),
                        "param_name" => "filter",
                        "value"     => array(
                            esc_html__("Hidden",'lucky')   => 'hidden',
                            esc_html__("Show",'lucky')   => 'show',
                            )
                    ),
                )
            ));
        }
    }
    add_action( 'wp_ajax_loadmore_product', 's7upf_loadmore_product' );
    add_action( 'wp_ajax_nopriv_loadmore_product', 's7upf_loadmore_product' );
    if(!function_exists('s7upf_loadmore_product')){
        function s7upf_loadmore_product() {
            $page = $_POST['page'];
            $load_data = $_POST['load_data'];
            $load_data = str_replace('\"', '"', $load_data);
            $load_data = json_decode($load_data,true);
            extract($load_data);
            $filter_data = $_POST['filter_data'];

            $args = array(
                'post_type'         => 'product',
                'posts_per_page'    => $number,
                'orderby'           => $order_by,
                'order'             => $order,
                'paged'             => $page + 1,
                );
            if($product_type == 'trendding'){
                $args['meta_query'][] = array(
                        'key'     => 'trending_product',
                        'value'   => 'on',
                        'compare' => '=',
                    );
            }
            if($product_type == 'toprate'){
                $args['meta_key'] = '_wc_average_rating';
                $args['orderby'] = 'meta_value_num';
                $args['meta_query'] = WC()->query->get_meta_query();
                $args['tax_query'][] = WC()->query->get_tax_query();
            }
            if($product_type == 'mostview'){
                $args['meta_key'] = 'post_views';
                $args['orderby'] = 'meta_value_num';
            }
            if($product_type == 'bestsell'){
                $args['meta_key'] = 'total_sales';
                $args['orderby'] = 'meta_value_num';
            }
            if($product_type=='onsale'){
                $args['meta_query']['relation']= 'OR';
                $args['meta_query'][]=array(
                    'key'   => '_sale_price',
                    'value' => 0,
                    'compare' => '>',                
                    'type'          => 'numeric'
                );
                $args['meta_query'][]=array(
                    'key'   => '_min_variation_sale_price',
                    'value' => 0,
                    'compare' => '>',                
                    'type'          => 'numeric'
                );
            }
            if($product_type == 'featured'){
                $args['meta_key'] = '_featured';
                $args['meta_value'] = 'yes';
            }
            if(!empty($cats)) {
                $custom_list = explode(",",$cats);
                $args['tax_query'][]=array(
                    'taxonomy'=>'product_cat',
                    'field'=>'slug',
                    'terms'=> $custom_list
                );
            }
            $tax_query = array();
            foreach ($data_filter as $key => $value) {
                if($key != 'category' && $key != 'price'){                    
                    $tax_query = array();
                    $tax_query['relation'] = 'AND';
                    $args['meta_query'] = array(array(
                                                'key'           => '_visibility',
                                                'value'         => array('catalog', 'visible'),
                                                'compare'       => 'IN'
                                            ));
                    $tax_query[] =  array(
                                                'taxonomy'      => 'pa_'.$key,
                                                'terms'         => $value,
                                                'field'         => 'slug',
                                                'operator'      => 'IN'
                                            );
                }
                if($key == 'category'){
                    $tax_query[]=array(
                                        'taxonomy' => 'product_cat',
                                        'field' => 'slug',
                                        'terms' => $value
                                    );
                }
            }
            if(isset($data_filter['price'])){
                if(!empty($data_filter['price'])){
                    $price_filter = explode(',', $data_filter['price']);
                    $min = $price_filter[0];
                    $max = $price_filter[1];
                    $args['post__in'] = sv_filter_price($min,$max);
                }
            }
            if(!empty($tax_query)) $args['tax_query'] = array_merge($tax_query,$args['tax_query']);
            $item_html = '';
            $product_query = new WP_Query($args);
            if($product_query->have_posts()) {
                while($product_query->have_posts()) {
                    $product_query->the_post();
                    global $product;
                    switch ($item_style) {
                        case 'list-product4':
                            $check = rand(1,10);
                            if($check % 2 == 0) $size = array(270,236);
                            else $size = array(270,335);
                            $item_html .=    '<div class="item-product-masonry">
                                                <div class="product-thumb">
                                                    <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                    <div class="product-info">
                                                        '.s7upf_product_link().'
                                                        '.s7upf_get_rating_html().'
                                                        <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                        '.s7upf_get_price_html().'
                                                    </div>
                                                </div>
                                            </div>';
                            break;
                        
                        default:
                            $item_html .=    '<div class="col-md-3 col-sm-4 col-xs-6">
                                                <div class="item-product">
                                                    <div class="product-thumb">
                                                        <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
                                                        '.s7upf_product_link().'
                                                        <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                        '.s7upf_get_rating_html().'
                                                        '.s7upf_get_saleoff_html().'
                                                    </div>
                                                    <div class="product-info">
                                                        <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                        '.s7upf_get_price_html().'
                                                    </div>
                                                </div>
                                            </div>';
                            break;
                    } 
                    $count++;
                }
            }
            echo balanceTags($item_html);
            wp_reset_postdata();
        }
    }
    add_action( 'wp_ajax_filter_product', 's7upf_filter_product' );
    add_action( 'wp_ajax_nopriv_filter_product', 's7upf_filter_product' );
    if(!function_exists('s7upf_filter_product')){
        function s7upf_filter_product() {
            $load_data = $_POST['load_data'];
            $load_data = str_replace('\"', '"', $load_data);            
            $data_loadjs = $load_data;
            $load_data = json_decode($load_data,true);
            extract($load_data);
            $data_filter = $_POST['filter_data'];

            $args = array(
                'post_type'         => 'product',
                'posts_per_page'    => $number,
                'orderby'           => $order_by,
                'order'             => $order,
                );
            if($product_type == 'trendding'){
                $args['meta_query'][] = array(
                        'key'     => 'trending_product',
                        'value'   => 'on',
                        'compare' => '=',
                    );
            }
            if($product_type == 'toprate'){
                $args['meta_key'] = '_wc_average_rating';
                $args['orderby'] = 'meta_value_num';
                $args['meta_query'] = WC()->query->get_meta_query();
                $args['tax_query'][] = WC()->query->get_tax_query();
            }
            if($product_type == 'mostview'){
                $args['meta_key'] = 'post_views';
                $args['orderby'] = 'meta_value_num';
            }
            if($product_type == 'bestsell'){
                $args['meta_key'] = 'total_sales';
                $args['orderby'] = 'meta_value_num';
            }
            if($product_type=='onsale'){
                $args['meta_query']['relation']= 'OR';
                $args['meta_query'][]=array(
                    'key'   => '_sale_price',
                    'value' => 0,
                    'compare' => '>',                
                    'type'          => 'numeric'
                );
                $args['meta_query'][]=array(
                    'key'   => '_min_variation_sale_price',
                    'value' => 0,
                    'compare' => '>',                
                    'type'          => 'numeric'
                );
            }
            if($product_type == 'featured'){
                $args['meta_key'] = '_featured';
                $args['meta_value'] = 'yes';
            }
            if(!empty($cats)) {
                $custom_list = explode(",",$cats);
                $args['tax_query'][]=array(
                    'taxonomy'=>'product_cat',
                    'field'=>'slug',
                    'terms'=> $custom_list
                );
            }
            $tax_query = array();
            foreach ($data_filter as $key => $value) {
                if($key != 'category' && $key != 'price' && $key != 'tab'){
                    $tax_query['relation'] = 'AND';
                    $args['meta_query'] = array(array(
                                                'key'           => '_visibility',
                                                'value'         => array('catalog', 'visible'),
                                                'compare'       => 'IN'
                                            ));
                    $tax_query[] =  array(
                                                'taxonomy'      => 'pa_'.$key,
                                                'terms'         => $value,
                                                'field'         => 'slug',
                                                'operator'      => 'IN'
                                            );
                }
                if($key == 'category'){
                    $tax_query[]=array(
                                        'taxonomy' => 'product_cat',
                                        'field' => 'slug',
                                        'terms' => $value
                                    );
                }
            }
            if(isset($data_filter['price'])){
                if(!empty($data_filter['price'])){
                    $price_filter = explode(',', $data_filter['price']);
                    $min = $price_filter[0];
                    $max = $price_filter[1];
                    $args['post__in'] = s7upf_filter_price($min,$max);
                }
            }
            if(!empty($tax_query)) $args['tax_query'] = $tax_query;
            $item_html = '';
            $product_query = new WP_Query($args);
            $max_page = $product_query->max_num_pages;
            if($product_query->have_posts()) {
                echo    '<div class="content-style-list '.esc_attr($item_style).'">
                            <div class="row content-load-ajax">';
                while($product_query->have_posts()) {
                    $product_query->the_post();
                    global $product;
                    switch ($item_style) {
                        case 'list-product4':
                            $check = rand(1,10);
                            if($check % 2 == 0) $size = array(270,236);
                            else $size = array(270,335);
                            $item_html .=    '<div class="item-product-masonry">
                                                <div class="product-thumb">
                                                    <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),$size).'</a>
                                                    <div class="product-info">
                                                        '.s7upf_product_link().'
                                                        '.s7upf_get_rating_html().'
                                                        <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                        '.s7upf_get_price_html().'
                                                    </div>
                                                </div>
                                            </div>';
                            break;
                        
                        default:
                            $item_html .=    '<div class="col-md-3 col-sm-4 col-xs-6">
                                                <div class="item-product">
                                                    <div class="product-thumb">
                                                        <a href="'.esc_url(get_the_permalink()).'" class="product-thumb-link">'.get_the_post_thumbnail(get_the_ID(),array(250,310)).'</a>
                                                        '.s7upf_product_link().'
                                                        <a data-product-id="'.get_the_id().'" href="'.esc_url(get_the_permalink()).'" class="product-quick-view quickview-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                                        '.s7upf_get_rating_html().'
                                                        '.s7upf_get_saleoff_html().'
                                                    </div>
                                                    <div class="product-info">
                                                        <h3 class="product-title"><a href="'.esc_url(get_the_permalink()).'" title="'.esc_attr(get_the_title()).'">'.get_the_title().'</a></h3>
                                                        '.s7upf_get_price_html().'
                                                    </div>
                                                </div>
                                            </div>';
                            break;
                    } 
                    $count++;
                }
                echo balanceTags($item_html);
                echo        '</div>
                        </div>';
                if($max_page > 1 && $load_more == 'show'){
                    echo    '<a href="#" class="load-ajax-btn btn-product-loadmore border radius" data-page="1" data-max_page="'.$max_page.'" data-load_data='."'".$data_loadjs.' '."'".'>'.esc_html__("Load more","lucky").'<i class="fa fa-spinner hidden" aria-hidden="true"></i></a>';
                }
                echo    '<input class="hidden load-ajax-btn" data-load_data='."'".$data_loadjs.' '."'".'>';
            }
            wp_reset_postdata();
        }
    }
}
