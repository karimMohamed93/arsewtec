<?php
/**
 * Created by Sublime text 2.
 * User: thanhhiep992
 * Date: 25/12/15
 * Time: 10:00 AM
 */

if(!function_exists('s7upf_vc_header_meta2'))
{
    function s7upf_vc_header_meta2($attr)
    {
        $html = '';
        extract(shortcode_atts(array(
            'style'           => '',
            'check_list'      => '',
            'account_content' => '',
            'wishlist_label'  => '',
            'wishlist_link'   => '',
        ),$attr));
        $check_array = explode(',', $check_list);
        $html .=    '<div class="top-right '.esc_attr($style).'">
                        <ul class="list-inline">';
        if(in_array('search', $check_array)){
            ob_start();
            $search_val = get_search_query();
            if(empty($search_val)){
                $search_val = esc_html__("Find amazing products","lucky");
            }
            ?>
            <form class="form-hidden" method="get" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
                <input name="s" onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="<?php echo esc_attr($search_val)?>" type="text">
                <input type="hidden" name="post_type" value="product" />
                <input value="<?php esc_attr_e("Search","lucky")?>" type="hidden">
            </form>
            <?php
            $search_html = ob_get_clean();
            $html .=    '<li class="info-search">
                            <a href="#" class="account-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                            '.$search_html.'
                        </li>';
        }
        if(in_array('user', $check_array)){
            $html .=        '<li class="info-user">
                                <a href="#" class="account-link"><i class="fa fa-user" aria-hidden="true"></i> '.esc_html__("My Account","lucky").'</a>
                                <ul class="list-unstyled inner-user-info">';
            parse_str(urldecode($account_content), $account_list);
            foreach ($account_list as $value) {
                $icon_html = '';
                if(!empty($value['icon'])){
                    if(strpos($value['icon'],'lnr') !== false) $icon_html = '<span class="lnr '.$value['icon'].'"></span>';
                    else $icon_html =   '<i class="fa '.$value['icon'].'"></i>';
                    $html .=            '<li><a href="'.esc_url($value['url']).'">'.$icon_html.' '.$value['title'].'</a></li>';
                }
                
            }        
            $html .=            '</ul>
                            </li>';
        }
        if(in_array('wishlist', $check_array)){
            $html .=        '<li><a href="'.esc_url($wishlist_link).'" class="wishlist-link"><i class="fa fa-heart" aria-hidden="true"></i> '.$wishlist_label.'</a></li>';
        }
        if(in_array('cart', $check_array)){
            $html_cart =        '<li class="info-cart cart-header-2">
                                <a href="#" class="mycart-link icon-cart"><i class="fa fa-opencart" aria-hidden="true"></i> '.esc_html__(" My Cart","lucky").' <span class="number-cart-total cart-item-count">0</span></a>
                                <div class="inner-cart-info">
                                    <h2><span class="cart-item-count">0</span> '.esc_html__("items","lucky").'</h2>
                                    <div class="mini-cart-content">'.s7upf_mini_cart().'</div>
                                </div>
                                <input id="num-decimal" type="hidden" value="'.get_option("woocommerce_price_num_decimals").'">
                                <input id="currency" type="hidden" value=".'.get_option("woocommerce_currency").'">
                            </li>';
            $html .= apply_filters('s7upf_tempalte_mini_cart',$html_cart);
        }
        $html .=        '</ul>
                    </div>';
        return $html;
    }
}

stp_reg_shortcode('sv_header_meta2','s7upf_vc_header_meta2');

vc_map( array(
    "name"      => esc_html__("SV Header Meta", 'lucky'),
    "base"      => "sv_header_meta2",
    "icon"      => "icon-st",
    "category"  => '7Up-theme',
    "params"    => array(
        array(
            "type" => "dropdown",
            "heading" => esc_html__("Style",'lucky'),
            "param_name" => "style",
            "value"     => array(
                esc_html__("Default",'lucky')     => '',
                esc_html__("Home 3",'lucky')     => 'box-meta3',
                esc_html__("Home 6",'lucky')     => 'box-meta6',
                esc_html__("Home 8",'lucky')     => 'box-meta8',
                )
        ),
        array(
            "type" => "checkbox",
            "holder" => "div",
            "heading" => esc_html__("Icon Display",'lucky'),
            "param_name" => "check_list",
            "value"     => array(
                esc_html__("Search",'lucky') => 'search',
                esc_html__("User Dropdown",'lucky') => 'user',
                esc_html__("Wishlist",'lucky') => 'wishlist',
                esc_html__("Mini Cart",'lucky') => 'cart',
                )
        ),
        array(
            "type" => "add_account_menu",
            "heading" => esc_html__("User Dropdown Links",'lucky'),
            "param_name" => "account_content",
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Wishlist Label",'lucky'),
            "param_name" => "wishlist_label",
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Wishlist Link",'lucky'),
            "param_name" => "wishlist_link",
        ),
    )
));