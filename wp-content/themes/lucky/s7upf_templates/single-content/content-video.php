<?php
$data = '';
if (get_post_meta(get_the_ID(), 'format_media', true)) {
    $media_url = get_post_meta(get_the_ID(), 'format_media', true);
    $data .='<div class="post-video">';
    $data .= s7upf_remove_w3c(wp_oembed_get($media_url));
    $data .='</div>';
}
?>
<div class="row">
    <div class="col-md-5 col-sm-6 col-xs-12">
        <h2 class="post-title"><?php the_title()?></h2>
        <div class="post-info">
            <?php s7upf_display_metabox()?>
            <div class="share-social">
                <label><?php esc_html_e("Share","lucky")?>:</label>
                <a href="http://www.facebook.com/sharer.php?u=<?php echo get_the_permalink();?>"><i class="fa fa-facebook-square"></i></a>
                <a href="http://twitthis.com/twit?url=<?php echo get_the_permalink();?>"><i class="fa fa-twitter-square"></i></a>
                <a href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>"><i class="fa fa-google-plus-square"></i></a>
                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php if(function_exists('the_post_thumbnail')) echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"><i class="fa fa-pinterest-square"></i></a>
            </div>
        </div>
        <div class="main-single-content">
        <?php
            $f_content = get_post_meta(get_the_ID(),'first_intro',true);
            echo balanceTags($f_content);
        ?>
        </div>
    </div>
    <div class="col-md-7 col-sm-6 col-xs-12">
        <?php if(!empty($data)) echo balanceTags($data);?>
    </div>
</div>
<div class="main-single-content">
    <?php the_content()?>
</div>