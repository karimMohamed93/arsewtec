<?php
/**
	* Template Name: homePage
	*/
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php 
$page = get_page(21);
$content = apply_filters('the_content',$page->post_content); 
$about_us_img=get_field('about_us_img',21);
?>
	
<div class="col-lg-12 col-xs-12 col-md-12 padding">
	  <!--  Demos -->
   <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="<?php echo get_template_directory_uri();?>/Images/Slideimg.jpg" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="<?php echo get_template_directory_uri();?>/Images/Slideimg.jpg" alt="Los Angeles" style="width:100%;">
      </div>
  
      <div class="item">
        <img src="<?php echo get_template_directory_uri();?>/Images/Slideimg.jpg" alt="Los Angeles" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<div class="col-lg-12 col-xs-12 col-md-12 padding Part_Companies">
	<div class="container">
		<div class="col-lg-12 col-xs-12 titles_company">
		<span>شركات </span>
			<span style="color: blue;">المجموعه</span>
		</div>
		<div class="col-lg-12 col-xs-12 col-md-12 imgsForCompanies">
		 <?php
     $term = get_term( get_post_meta(get_the_id(),'GroupCompanys',true) ,'GroupCompanys' );
            $type = 'GroupCompanys';
            $args=array(
              'post_type' => $type,
              'post_status' => 'publish',
              'posts_per_page' => -1,
              'caller_get_posts'=> 1
            );
            $my_query = null;
            $my_query = new WP_Query($args);

            if( $my_query->have_posts() ) { 
            while($my_query->have_posts()) : $my_query->the_post();
              $ID=get_the_id();
              
 
             $logo_image=get_field('company_group_img');  
            
        ?>
			<div class="col-lg-4 col-xs-12 col-md-4 OneItem float_right" style="background-image: url('<?=$logo_image;?>');">
				
			</div>
  <?php   endwhile; }?>

			
		</div>
	</div>
</div>
<div class="col-lg-12"><br></div>
<div class="col-lg-12 col-xs-12 col-md-12 padding El2ola">
<div class="container">
<div class="col-lg-12 col-xs-12 col-md-12">
<div class="col-lg-6 col-xs-12 col-md-6 float_right">
<div class="col-lg-12 col-xs-12 titles_company">
		<span>الاولى  </span>
			<span style="color: blue;">التمويل العقارى</span>
		</div>


<div class="col-lg-12 col-xs-12 col-md-12 company_detail">
<p>
<?=$content?>
</p>
<button class="button btn ComBtn">
	قسط وحدتك الان
</button>
	
</div>

</div>
<div class="col-lg-6 col-xs-12 col-md-6 float_right imgcompany" >
<img src="<?=$about_us_img;?>">

</div>

</div>
</div>

</div>
<div class="col-lg-12"><br></div>

	

<?php get_footer(); ?>
