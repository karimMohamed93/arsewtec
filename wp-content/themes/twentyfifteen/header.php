<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/droid-arabic-kufi" type="text/css"/>
<script src="<?php echo get_template_directory_uri();?>/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href=".<?php echo get_template_directory_uri();?>/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/css/task.css">
<!-- <link rel="stylesheet" href="../TechnicalTask/OwlCarousel2-2.2.1/OwlCarousel2-2.2.1/docs/assets/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="../TechnicalTask/OwlCarousel2-2.2.1/OwlCarousel2-2.2.1/docs/assets/owlcarousel/assets/owl.theme.default.min.css">
     <!-- javascript -->
    <!--<script src="../TechnicalTask/OwlCarousel2-2.2.1/OwlCarousel2-2.2.1/docs/assets/vendors/jquery.min.js"></script>
    <script src="../TechnicalTask/OwlCarousel2-2.2.1/OwlCarousel2-2.2.1/docs/assets/owlcarousel/owl.carousel.js"></script>-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">


<title>Home Page</title>
</head>

<body <?php body_class(); ?>>
<header>
	<div class="container-fluid">
	 <div class="container">
	 <div class="col-lg-12 col-xs-12 col-md-12">
	 	<div class="col-lg-2 col-xs-10 col-md-2 float_right">
	 	  <img src="<?php echo get_template_directory_uri();?>/Images/logo.png">
	 	</div>

	 	<div class="col-lg-8 col-xs-2 col-md-8 float_right">
	 	<div class="col-lg-12 col-xs-12 col-md-12 callUs float_right">

	 	<div class="col-lg-8  float_right">
	 	
	 	</div>
	 	<div class="col-lg-4 float_right hidden-xs hidden-sm"><img src="<?php echo get_template_directory_uri();?>/Images/Hotline icon.png"></div>
	 
	 	</div>
	<div id="mySidenav" class="sidenav ">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
</div>

	 	<div class="col-lg-12  col-md-12 Page_Header float_right  hidden-sm hidden-xs">

	<?php 
	 echo wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav navbar-nav' , 'menu' => 'Header' ,) );  ?>
	 	</div>
	 	<div class="col-xs-12 hidden-lg visible-xs">
	 		<span style="font-size:50px;cursor:pointer" onclick="openNav()">&#9776; </span>

	 	</div>
	 	
	 	</div>
	 	
	 </div>

	 	
	 </div>
	</div>
		
	
</header>
	<div id="content" class="site-content">
<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>